#include "Application.h"
#include "imgui.h"
#include "imgui_internal.h"
#include "misc/cpp/imgui_stdlib.h"

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/PointIndices.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/region_growing.h>
#include <pcl/segmentation/cpc_segmentation.h>
#include <pcl/segmentation/lccp_segmentation.h>

#include <pcl/features/boundary.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/search/search.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/common/common.h>
#include <pcl/io/ply_io.h>

#include <pcl/surface/poisson.h>
#include <pcl/point_types.h>

#include <pcl/registration/registration.h>
#include <pcl/registration/transformation_estimation_3point.h>
#include <pcl/registration/icp.h>

#include <chrono>
#include <thread>

#include <open3d/Open3D.h>

#include "lib/ImGuiFileDialog/ImGuiFileDialog.h"



//initialize statics as in https://stackoverflow.com/questions/3698043/static-variables-in-c
int MyApp::NORMAL_ESTIMATION_OMP_DATA::NrThreads = 2;
float MyApp::NORMAL_ESTIMATION_OMP_DATA::ViewPointX = .0f;
float MyApp::NORMAL_ESTIMATION_OMP_DATA::ViewPointY = .0f;
float MyApp::NORMAL_ESTIMATION_OMP_DATA::ViewPointZ = .0f;
double MyApp::NORMAL_ESTIMATION_OMP_DATA::RadiusSearch = 0.01;
int MyApp::NORMAL_ESTIMATION_OMP_DATA::KSearch = 10;
bool MyApp::NORMAL_ESTIMATION_OMP_DATA::BNrThreads = true;
bool MyApp::NORMAL_ESTIMATION_OMP_DATA::BViewPoint = false;
bool MyApp::NORMAL_ESTIMATION_OMP_DATA::BUseSensorOriginAsViewPoint = false;
bool MyApp::NORMAL_ESTIMATION_OMP_DATA::BRadiusSearch = false;
bool MyApp::NORMAL_ESTIMATION_OMP_DATA::BKSearch = true;
std::string MyApp::NORMAL_ESTIMATION_OMP_DATA::cldNrmlFlPth = "";

std::string MyApp::FILEPATH_STORAGE::ultimate = ""; // is this struct really needed?
std::string MyApp::FILEPATH_STORAGE::penultimate = "";
std::string MyApp::FILEPATH_STORAGE::penpenultimate = "";
std::string MyApp::FILEPATH_STORAGE::penpenpenultimate = "";
std::string MyApp::FILEPATH_STORAGE::penpenpenpenultimate = "";

namespace MyApp
{
    struct O3D_POISSON_SURFACE_DATA {
        const open3d::geometry::PointCloud pcd;
        int depth;
        float width;
        float scale;
        bool linear_fit;
        int n_threads;

        //bool hasNEOMPdata = false;

        // bool values to determine whether certain functions in
        // PCLPoissonSurfaceReconstruction() are going to be
        // executed at runtime
        bool Bdepth;
        bool Bwidth;
        bool Bscale;
        bool Bn_threads;
        bool BvisRes;
    };

    bool O3DPoissonSurfaceReconstruction(O3D_POISSON_SURFACE_DATA data,
        std::string outputFlNm, std::string outputFlPth, std::string cldFlPth, std::string cldNrmlFlPth)
    {

        auto Test_PTR = std::make_shared<open3d::geometry::PointCloud>();
        open3d::io::ReadPointCloud(cldFlPth, *Test_PTR);
        Test_PTR->EstimateNormals(); // replace with cldNrmlFlPth
        int depth;
        float width;
        float scale;
        int n_threads;
        (data.Bdepth) ? depth = data.depth : depth = 8;
        (data.Bwidth) ? width = data.width : width = 0.0f;
        (data.Bscale) ? scale = data.scale : scale = 1.1f;
        (data.Bn_threads) ? n_threads = data.n_threads : n_threads = -1;

        auto mesh = open3d::geometry::TriangleMesh::CreateFromPointCloudPoisson(*Test_PTR, depth, width,
            scale,data.linear_fit, n_threads);

        // mask out density values smaller 0.1 - usefull to trim extrapolated part of mesh
        //std::vector<bool> mask;
        //for (int i = 0; i < std::get<1>(mesh).size(); i++) {
        //    (std::get<1>(mesh).at(i) < 4) ? mask.push_back(1) : mask.push_back(0);
        //    std::cout << std::get<1>(mesh).at(i) << "    " << mask.at(i) << std::endl;
        //}
        //std::get<0>(mesh)->RemoveVerticesByMask(mask);
        if (data.BvisRes) {
            open3d::visualization::DrawGeometries({ std::get<0>(mesh) });
        }
            
        
        if (!std::get<0>(mesh)->IsEmpty()) {
            open3d::io::WriteTriangleMeshToPLY(outputFlPth + "\\" + outputFlNm + "_O3DPSR.ply", *(std::get<0>(mesh)), true, false, true, true, false, false);
            return true;
        }
        else {
            return false;
        }
    }

    // shift array to the left to update combobox
    void updateFilepathStorage(std::string pths[5], std::string newPth) {
        for (int i = 4; i > 0; i--) {
            pths[i] = pths[i - 1];
        };
        pths[0] = newPth;
    }

    // remove ultimate item (when selected 'delete NE after success')
    void updateFilepathStorageRmUltmt(std::string pths[5]) {
        for (int i = 0; i < 4; i++) {
            pths[i] = pths[i + 1];
        };
        pths[4] = "";
    }

    void FileHelper(const char* file) {
        ImGuiFileDialog::Instance()->OpenDialog("ChooseFileDlgKey", "Choose File", ".pcd", ".");
        if (ImGuiFileDialog::Instance()->Display("ChooseFileDlgKey"))
        {
            // action if OK
            if (ImGuiFileDialog::Instance()->IsOk())
            {
                std::string filePathName = ImGuiFileDialog::Instance()->GetFilePathName();
                std::string filePath = ImGuiFileDialog::Instance()->GetCurrentPath();
                file = (filePath + filePathName).c_str();
            }

            // close
            ImGuiFileDialog::Instance()->Close();
        }
    }

    bool switchSlctbl(int selected, int subSelected) {
        if (selected == 3 || selected == 2 || (selected == 0 && subSelected == 1))
            return false;
        else
            return true;
    }

    bool normalArrayEmpty(std::string arr[], int size) {
        bool check = true;
        for (int i = 0; i < size; i++) {
            check = check && arr[i].empty();
        }
        return check;
    }

    void InputHelperInt(static bool& Bset, static int& Iset, const char* Clabel, const char* Ilabel) {
        ImGui::Checkbox(Clabel, &Bset);
        ImGui::SameLine();
        if (!Bset)
            ImGui::BeginDisabled();
        ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.25f);
        //ImGui::InputInt(Ilabel, &Iset);
        ImGui::InputInt(Ilabel, &Iset, 0);
        if (!Bset) {
            ImGui::EndDisabled();
        }
    }

    void InputHelperFlt(static bool& Bset, static float& Fset, const char* Clabel, const char* Flabel) {
        ImGui::Checkbox(Clabel, &Bset);
        ImGui::SameLine();
        if (!Bset)
            ImGui::BeginDisabled();
        ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.25f);
        ImGui::InputFloat(Flabel, &Fset, 0.0f,0.0f, "%.3f");
        if (!Bset) {
            ImGui::EndDisabled();
        }
    }

    void InputHelperDbl(static bool& Bset, static double& Dset, const char* Clabel, const char* Flabel) {
        ImGui::Checkbox(Clabel, &Bset);
        ImGui::SameLine();
        if (!Bset)
            ImGui::BeginDisabled();
        ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.25f);
        ImGui::InputDouble(Flabel, &Dset);
        if (!Bset) {
            ImGui::EndDisabled();
        }
    }

    void InputHelperIntSld(static bool& Bset, static int& Iset, const char* Clabel, const char* Ilabel, static int Imax) {
        ImGui::Checkbox(Clabel, &Bset);
        ImGui::SameLine();
        if (!Bset)
            ImGui::BeginDisabled();
        ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.25f);
        ImGui::InputInt(Ilabel, &Iset);
        ImGui::SliderInt(Ilabel, &Iset, 0, Imax);
        if (!Bset) {
            ImGui::EndDisabled();
        }
    }
    
    void InputHelperFltSld(static bool& Bset, static float& Fset, const char* Clabel, const char* Flabel, float Fmax) {
        ImGui::Checkbox(Clabel, &Bset);
        ImGui::SameLine();
        if (!Bset)
            ImGui::BeginDisabled();
        ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.25f);
        ImGui::SliderFloat(Flabel, &Fset, 0.0f, Fmax);
        if (!Bset) {
            ImGui::EndDisabled();
        }
    }

    /*    void InputHelperDblSld(static bool& Bset, static double& Dset, const char* Clabel, const char* Dlabel, static float& Dmax) {
        ImGui::Checkbox(Clabel, &Bset);
        ImGui::SameLine();
        if (!Bset)
            ImGui::BeginDisabled();
        ImGui::SliderDouble(Dlabel, &Dset, 0.0, Dmax);
        if (!Bset) {
            ImGui::EndDisabled();
        }
    }*/

    struct ExampleAppLog
    {
        ImGuiTextBuffer     Buf;
        ImGuiTextFilter     Filter;
        ImVector<int>       LineOffsets; // Index to lines offset. We maintain this with AddLog() calls.
        bool                AutoScroll;  // Keep scrolling if already at the bottom.

        ExampleAppLog()
        {
            AutoScroll = true;
            Clear();
        }

        void    Clear()
        {
            Buf.clear();
            LineOffsets.clear();
            LineOffsets.push_back(0);
        }

        void    AddLog(const char* fmt, ...) IM_FMTARGS(2)
        {
            int old_size = Buf.size();
            va_list args;
            va_start(args, fmt);
            Buf.appendfv(fmt, args);
            va_end(args);
            for (int new_size = Buf.size(); old_size < new_size; old_size++)
                if (Buf[old_size] == '\n')
                    LineOffsets.push_back(old_size + 1);
        }

        void    Draw(const char* title, bool* p_open = NULL)
        {
            ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize;
            if (!ImGui::Begin(title, p_open, window_flags))
            {
                ImGui::End();
                return;
            }

            // Options menu
            /*if (ImGui::BeginPopup("Options"))
            {
                ImGui::Checkbox("Auto-scroll", &AutoScroll);
                ImGui::EndPopup();
            }*/

            // Main window
            //if (ImGui::Button("Options"))
            //    ImGui::OpenPopup("Options");
            //ImGui::SameLine();
            //bool clear = ImGui::Button("Clear");
            //ImGui::SameLine();
            //bool copy = ImGui::Button("Copy");
            //ImGui::SameLine();
            //Filter.Draw("Filter", -100.0f);

            //ImGui::Separator();
            ImGui::BeginChild("scrolling", ImVec2(0, 0), false, ImGuiWindowFlags_HorizontalScrollbar);

            //if (clear)
            //    Clear();
            //if (copy)
            //    ImGui::LogToClipboard();

            ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0, 0));

            const char* buf = Buf.begin();
            const char* buf_end = Buf.end();
            if (Filter.IsActive())
            {
                // In this example we don't use the clipper when Filter is enabled.
                // This is because we don't have a random access on the result on our filter.
                // A real application processing logs with ten of thousands of entries may want to store the result of
                // search/filter.. especially if the filtering function is not trivial (e.g. reg-exp).
                for (int line_no = 0; line_no < LineOffsets.Size; line_no++)
                {
                    const char* line_start = buf + LineOffsets[line_no];
                    const char* line_end = (line_no + 1 < LineOffsets.Size) ? (buf + LineOffsets[line_no + 1] - 1) : buf_end;
                    if (Filter.PassFilter(line_start, line_end))
                        ImGui::TextUnformatted(line_start, line_end);
                }
            }
            else
            {
                // The simplest and easy way to display the entire buffer:
                //   ImGui::TextUnformatted(buf_begin, buf_end);
                // And it'll just work. TextUnformatted() has specialization for large blob of text and will fast-forward
                // to skip non-visible lines. Here we instead demonstrate using the clipper to only process lines that are
                // within the visible area.
                // If you have tens of thousands of items and their processing cost is non-negligible, coarse clipping them
                // on your side is recommended. Using ImGuiListClipper requires
                // - A) random access into your data
                // - B) items all being the  same height,
                // both of which we can handle since we an array pointing to the beginning of each line of text.
                // When using the filter (in the block of code above) we don't have random access into the data to display
                // anymore, which is why we don't use the clipper. Storing or skimming through the search result would make
                // it possible (and would be recommended if you want to search through tens of thousands of entries).
                ImGuiListClipper clipper;
                clipper.Begin(LineOffsets.Size);
                while (clipper.Step())
                {
                    for (int line_no = clipper.DisplayStart; line_no < clipper.DisplayEnd; line_no++)
                    {
                        const char* line_start = buf + LineOffsets[line_no];
                        const char* line_end = (line_no + 1 < LineOffsets.Size) ? (buf + LineOffsets[line_no + 1] - 1) : buf_end;
                        ImGui::TextUnformatted(line_start, line_end);
                    }
                }
                clipper.End();
            }

            ImGui::PopStyleVar();

            if (AutoScroll && ImGui::GetScrollY() >= ImGui::GetScrollMaxY())
                ImGui::SetScrollHereY(1.0f);

            ImGui::EndChild();
            ImGui::End();
        }
    };

    static void HelpMarker(const char* desc)
    {
        ImGui::TextDisabled("(?)");
        if (ImGui::IsItemHovered())
        {
            ImGui::BeginTooltip();
            ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
            ImGui::TextUnformatted(desc);
            ImGui::PopTextWrapPos();
            ImGui::EndTooltip();
        }
    }

    void DockSpace() {
        static bool opt_fullscreen = true;
        static bool opt_padding = false;
        //static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;
        static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_NoResize;

        // We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable into,
        // because it would be confusing to have two docking targets within each others.
        ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
        if (opt_fullscreen)
        {
            const ImGuiViewport* viewport = ImGui::GetMainViewport();
            ImGui::SetNextWindowPos(viewport->WorkPos);
            ImGui::SetNextWindowSize(viewport->WorkSize);
            ImGui::SetNextWindowViewport(viewport->ID);
            ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
            ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
            window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
        }
        else
        {
            dockspace_flags &= ~ImGuiDockNodeFlags_PassthruCentralNode;
        }

        // When using ImGuiDockNodeFlags_PassthruCentralNode, DockSpace() will render our background
        // and handle the pass-thru hole, so we ask Begin() to not render a background.
        if (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode)
            window_flags |= ImGuiWindowFlags_NoBackground;

        // Important: note that we proceed even if Begin() returns false (aka window is collapsed).
        // This is because we want to keep our DockSpace() active. If a DockSpace() is inactive,
        // all active windows docked into it will lose their parent and become undocked.
        // We cannot preserve the docking relationship between an active window and an inactive docking, otherwise
        // any change of dockspace/settings would lead to windows being stuck in limbo and never being visible.
        if (!opt_padding)
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
        ImGui::Begin("DockSpace Demo", nullptr, window_flags);
        if (!opt_padding)
            ImGui::PopStyleVar();

        if (opt_fullscreen)
            ImGui::PopStyleVar(2);

        // Submit the DockSpace
        ImGuiIO& io = ImGui::GetIO();
        if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable)
        {
            ImGuiID dockspace_id = ImGui::GetID("MyDockSpace");
            ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
        }
    }

    void dlgFile(std::string& filePathName)
    {
        // open Dialog Simple
        if (ImGui::Button("Open File Dialog"))
            ImGuiFileDialog::Instance()->OpenDialog("ChooseFileDlgKey", "Choose File", ".cpp,.h,.hpp", ".");

        // display
        if (ImGuiFileDialog::Instance()->Display("ChooseFileDlgKey"))
        {
            // action if OK
            if (ImGuiFileDialog::Instance()->IsOk())
            {
                filePathName = ImGuiFileDialog::Instance()->GetFilePathName();
                //filePath = ImGuiFileDialog::Instance()->GetCurrentPath();
                // action
            }

            // close
            ImGuiFileDialog::Instance()->Close();
        }
    }

    void dlgDir(std::string& filePath)
    {
        // open Dialog Simple
        if (ImGui::Button("Open File Dialog"))
            ImGuiFileDialog::Instance()->OpenDialog("ChooseFileDlgKey", "Choose File", ".cpp,.h,.hpp", ".");

        // display
        if (ImGuiFileDialog::Instance()->Display("ChooseFileDlgKey"))
        {
            // action if OK
            if (ImGuiFileDialog::Instance()->IsOk())
            {
                //filePathName = ImGuiFileDialog::Instance()->GetFilePathName();
                filePath = ImGuiFileDialog::Instance()->GetCurrentPath();
                // action
            }

            // close
            ImGuiFileDialog::Instance()->Close();
        }
    }

    bool PCLNormalEstimationOMP(
        std::string outputFlNm, std::string& outputFlPth, std::string cldFlPth) {
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>());
        pcl::io::loadPCDFile<pcl::PointXYZ>(cldFlPth, *cloud);

        pcl::NormalEstimationOMP<pcl::PointXYZ, pcl::Normal> ne;
        pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>());
        ne.setNumberOfThreads(MyApp::NORMAL_ESTIMATION_OMP_DATA::NrThreads);
        ne.setInputCloud(cloud);

        if (MyApp::NORMAL_ESTIMATION_OMP_DATA::BRadiusSearch && !MyApp::NORMAL_ESTIMATION_OMP_DATA::BKSearch) {
            ne.setRadiusSearch(MyApp::NORMAL_ESTIMATION_OMP_DATA::RadiusSearch);
        }
        else if (!MyApp::NORMAL_ESTIMATION_OMP_DATA::BRadiusSearch && MyApp::NORMAL_ESTIMATION_OMP_DATA::BKSearch) {
            ne.setKSearch(MyApp::NORMAL_ESTIMATION_OMP_DATA::KSearch);
        }
        else {
            return false;
        }


        if (MyApp::NORMAL_ESTIMATION_OMP_DATA::BViewPoint && !(MyApp::NORMAL_ESTIMATION_OMP_DATA::BUseSensorOriginAsViewPoint)) {
            ne.setViewPoint(MyApp::NORMAL_ESTIMATION_OMP_DATA::ViewPointX,
                MyApp::NORMAL_ESTIMATION_OMP_DATA::ViewPointY, MyApp::NORMAL_ESTIMATION_OMP_DATA::ViewPointZ);
        }
        else if (!MyApp::NORMAL_ESTIMATION_OMP_DATA::BViewPoint && MyApp::NORMAL_ESTIMATION_OMP_DATA::BUseSensorOriginAsViewPoint) {
            ne.useSensorOriginAsViewPoint();
        }
        // if both are selected inambiguous --> should be ruled out beforehand ...
        // workaround: take neither of the input and centroid instead
        else {
            Eigen::Vector4f centroid;
            pcl::compute3DCentroid(*cloud, centroid);
            ne.setViewPoint(centroid[0], centroid[1], centroid[2]);
        }
        ne.compute(*cloud_normals);


        ////scale normals TODO: make switchable, right now it is ALWAYS scaled
        //// WA: scaling only in PSR implemented since it NE selection in ComboBox is relying on '_NE.pcd' at file name ... quite instable at the moment! Therefore implemented at PCLPoissonSurfaceReconstruction
        // TODO: create like a switch in NORMAL_ESTIMATION_OMP_DATA to account for this ...
        //pcl::PointCloud<pcl::Normal>::Ptr cloud_normals_sc(new pcl::PointCloud<pcl::Normal>());
        //for (auto& normal : cloud_normals->points) {
        //    cloud_normals_sc->push_back(pcl::Normal(normal.normal_x * normal.curvature, normal.normal_y * normal.curvature, normal.normal_z * normal.curvature, normal.curvature));
        //}


        if (!((*cloud_normals).points.empty())) {
            pcl::io::savePCDFile((outputFlPth + "\\" + outputFlNm + "_NE.pcd"), *cloud_normals);
            return true;
        }
        else {
            return false;
        }
    }

    struct POISSON_SURFACE_DATA {
        int PoissonDepth;
        int PoissonMinDepth;
        float PoissonPointWeight;
        float PoissonScale;
        int PoissonSolverDividede;
        int PoissonIsoDivide;
        float PoissonSamplesPerNode;
        bool PoissonConfidence;
        bool PoissonManifold;
        bool PoissonOutputPolygons;
        int PoissonDegree;
        int PoissonThreads;

        bool hasNEOMPdata = false;

        // bool values to determine whether certain functions in
        // PCLPoissonSurfaceReconstruction() are going to be
        // executed at runtime
        bool BPoissonDepth;
        bool BPoissonMinDepth;
        bool BPoissonPointWeight;
        bool BPoissonScale;
        bool BPoissonSolverDividede;
        bool BPoissonIsoDivide;
        bool BPoissonSamplesPerNode;
        bool BPoissonDegree;
        bool BPoissonThreads;

        bool BScaleNormals; // WA: query if unit normals or normals scaled with curvature should be used (confidence flag); TODO: refactor
    };

    bool PCLPoissonSurfaceReconstruction(POISSON_SURFACE_DATA& data,
        std::string outputFlNm, std::string& outputFlPth, std::string cldFlPth, std::string cldNrmlFlPth) {

        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>());
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>());
        pcl::io::loadPCDFile<pcl::PointXYZ>(cldFlPth, *cloud);

        pcl::PassThrough<pcl::PointXYZ> filter;
        filter.setInputCloud(cloud);
        filter.filter(*cloud_filtered);

        Eigen::Vector4f centroid;
        pcl::compute3DCentroid(*cloud_filtered, centroid);

        pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>());
        pcl::io::loadPCDFile<pcl::Normal>(cldNrmlFlPth, *cloud_normals);

        for (size_t i = 0; i < cloud_normals->size(); ++i) {
            cloud_normals->points[i].normal_x *= -1;
            cloud_normals->points[i].normal_y *= -1;
            cloud_normals->points[i].normal_z *= -1;
        }

        // THIS IS JUST A WORKAROUND. LATER THE SCALING OF NORMALS SHOULD HAPPEN AT PCLNormalEstimationOMP!
        pcl::PointCloud<pcl::PointNormal>::Ptr cloud_smoothed_normals(new pcl::PointCloud<pcl::PointNormal>());
        if (data.BScaleNormals)
        {
            pcl::PointCloud<pcl::Normal>::Ptr cloud_normals_sc(new pcl::PointCloud<pcl::Normal>());
            for (auto& normal : cloud_normals->points) {
                cloud_normals_sc->push_back(pcl::Normal(normal.normal_x * normal.curvature, normal.normal_y * normal.curvature, normal.normal_z * normal.curvature, normal.curvature));
            }
#ifdef _DEBUG
            // scaled normal file
            pcl::io::savePCDFile((outputFlPth + "\\" + outputFlNm + "_NE_sc.pcd"), *cloud_normals_sc);
#endif
            pcl::concatenateFields(*cloud_filtered, *cloud_normals_sc, *cloud_smoothed_normals);     
        }
        else {
            pcl::concatenateFields(*cloud_filtered, *cloud_normals, *cloud_smoothed_normals);
        }

        pcl::Poisson<pcl::PointNormal> psr;
        pcl::PolygonMesh mesh;

        psr.setInputCloud(cloud_smoothed_normals);
        psr.setDepth(data.PoissonDepth);
        if (data.BPoissonMinDepth) { psr.setMinDepth(data.PoissonMinDepth); };
        if (data.BPoissonPointWeight) { psr.setPointWeight(data.PoissonPointWeight); };
        if (data.BPoissonScale) { psr.setScale(data.PoissonScale); };
        if (data.BPoissonSolverDividede) { psr.setSolverDivide(data.PoissonSolverDividede); };
        if (data.BPoissonIsoDivide) { psr.setIsoDivide(data.PoissonIsoDivide); };
        if (data.BPoissonSamplesPerNode) { psr.setSamplesPerNode(data.PoissonSamplesPerNode); };
        if (data.BPoissonDegree) { psr.setDegree(data.PoissonDegree); };
        if (data.BPoissonThreads) { psr.setThreads(data.PoissonThreads); };

        if (data.PoissonConfidence) { psr.setConfidence(data.PoissonConfidence); };
        if (data.PoissonManifold) { psr.setManifold(data.PoissonManifold); };
        if (data.PoissonOutputPolygons) { psr.setOutputPolygons(data.PoissonOutputPolygons); };

        psr.reconstruct(mesh);

        if (!mesh.cloud.data.empty()) {
            pcl::io::savePLYFile((outputFlPth + "\\" + outputFlNm + "_PSR.ply"), mesh);
            return true;
        }
        else {
            return false;
        }
    }

    struct BOUNDARY_ESTIMATION_DATA {
        double RadiusSearch;
    };

    bool PCLBoundaryEstimation(BOUNDARY_ESTIMATION_DATA& data,
        std::string outputFlNm, std::string& outputFlPth, std::string cldFlPth, std::string cldNrmlFlPth) {

        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>());
        pcl::io::loadPCDFile<pcl::PointXYZ>(cldFlPth, *cloud);

        pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>());
        pcl::io::loadPCDFile<pcl::Normal>(cldNrmlFlPth, *cloud_normals);

        pcl::BoundaryEstimation<pcl::PointXYZ, pcl::Normal, pcl::Boundary> be;
        be.setInputCloud(cloud);
        be.setInputNormals(cloud_normals);
        be.setRadiusSearch(data.RadiusSearch);
        be.setSearchMethod(typename pcl::search::KdTree<pcl::PointXYZ>::Ptr(new pcl::search::KdTree<pcl::PointXYZ>));
        pcl::PointCloud<pcl::Boundary> boundaries;
        be.compute(boundaries);
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_boundary(new pcl::PointCloud<pcl::PointXYZ>);
        if (!(boundaries.points.empty())) {
            for (int i = 0; i < boundaries.points.size(); i++) {
                if (boundaries.points.at(i).boundary_point == 1) {
                    cloud_boundary->push_back(cloud->at(i));
                }
            }
            pcl::io::savePCDFile((outputFlPth + "\\" + outputFlNm + "_BE.pcd"), *cloud_boundary);
            return true;
        }
        else
            return false;
    }

    struct SAC_SEGMENTATION_DATA {
        int SACMethod;
        int SACModel;
        int MaxIter;
        double Threshold;
        double Prob;
        int NrThreads;
        double RadiusLimit;
        double RadiusLimitMin;
        double RadiusLimitMax;
        double SamplesMaxDist;
        double EpsilonAngle;
        bool OptimizeCoefficients;
        bool NegativeInd;

        // bool values to determine whether certain functions in
        // PCLSACSegmentation() are going to be executed at runtime
        bool BMaxIter;
        bool BThreshold;
        bool BProb;
        bool BNrThreads;
        bool BRadiusLimit;
        bool BSamplesMaxDist;
        bool BEpsilonAngle;
    };

    bool PCLSACSegmentation(SAC_SEGMENTATION_DATA data,
        std::string outputFlNm, std::string& outputFlPth, std::string cldFlPth) {
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>());
        pcl::io::loadPCDFile<pcl::PointXYZ>(cldFlPth, *cloud);

        pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients());
        pcl::PointIndices::Ptr inliers(new pcl::PointIndices());
        pcl::SACSegmentation<pcl::PointXYZ> seg;

        seg.setModelType(data.SACModel);
        seg.setMethodType(data.SACMethod);
        seg.setOptimizeCoefficients(data.OptimizeCoefficients);
        seg.setDistanceThreshold(data.Threshold);
        seg.setInputCloud(cloud);

        if (data.BProb) { seg.setProbability(data.Prob); };
        if (data.BNrThreads) { seg.setNumberOfThreads(data.NrThreads); };
        if (data.BRadiusLimit) { seg.setRadiusLimits(data.RadiusLimitMin, data.RadiusLimitMax); };
        //if (data.BSamplesMaxDist) { seg.setSamplesMaxDist(data.SamplesMaxDist,??????); };
        if (data.BEpsilonAngle) { seg.setEpsAngle(data.EpsilonAngle); };

        seg.segment(*inliers, *coefficients);

        pcl::ExtractIndices<pcl::PointXYZ> extract = pcl::ExtractIndices<pcl::PointXYZ>();
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p(new pcl::PointCloud<pcl::PointXYZ>);
        extract.setInputCloud(cloud);
        extract.setIndices(inliers);
        extract.setNegative(data.NegativeInd);
        extract.filter(*cloud_p);

        if (!(*cloud_p).empty()) {
            pcl::io::savePCDFile((outputFlPth + "\\" + outputFlNm + "_SAC.pcd"), *cloud_p);
            return true;
        }
        else {
            return false;
        }
    }

    struct RG_SEGMENTATION_DATA {
        int MinClusterSize;
        int MaxClusterSize;
        int NumNeighbors;
        float SmoothThreshold;
        float CurvatureThreshold;
        float ResidualThreshold;

        bool BSmoothModeFlag;
        bool BCurvatureTestFlag;
        bool BResidualTestFlag;
        bool BNumNeighbors;
        bool BMaxClusterSize;
        bool BMinClusterSize;
    };

    bool PCLRGSegmentation(RG_SEGMENTATION_DATA data,
        std::string outputFlNm, std::string& outputFlPth, std::string cldFlPth, std::string cldNrmlFlPth) {
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>());
        pcl::io::loadPCDFile<pcl::PointXYZ>(cldFlPth, *cloud);

        pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>());
        pcl::io::loadPCDFile<pcl::Normal>(cldNrmlFlPth, *cloud_normals);

        pcl::search::Search<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
        pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> rgnGrw = pcl::RegionGrowing< pcl::PointXYZ, pcl::Normal>();


        rgnGrw.setSearchMethod(tree);

        (data.BMinClusterSize) ? rgnGrw.setMinClusterSize(data.MinClusterSize) : rgnGrw.setMinClusterSize(5);
        (data.BMaxClusterSize) ? rgnGrw.setMaxClusterSize(data.MaxClusterSize) : rgnGrw.setMaxClusterSize(1e6);
        (data.BNumNeighbors) ? rgnGrw.setNumberOfNeighbours(data.NumNeighbors) : rgnGrw.setNumberOfNeighbours(30);
        (data.BSmoothModeFlag) ? rgnGrw.setSmoothnessThreshold(data.SmoothThreshold) : rgnGrw.setSmoothModeFlag(false);
        (data.BCurvatureTestFlag) ? rgnGrw.setCurvatureThreshold(data.CurvatureThreshold) : rgnGrw.setCurvatureTestFlag(false);
        (data.BResidualTestFlag) ? rgnGrw.setResidualThreshold(data.ResidualThreshold) : rgnGrw.setResidualTestFlag(false);

        rgnGrw.setInputCloud(cloud);
        //reg.setIndices (indices);
        rgnGrw.setInputNormals(cloud_normals);

        //try {
        //    pcl::PointCloud <pcl::PointXYZRGB>::Ptr colored_cloud = rgnGrw.getColoredCloud();   //TODO: why crash at smooth = pi ?
        //}
        //catch (...) {   // TODO: find specific error to catch
        //    return false;   // TODO: int return to specify which error caused the issue --> EXIT_FAILURE with int
        //}

        std::vector <pcl::PointIndices> clusters;
        rgnGrw.extract(clusters);
        pcl::PointCloud <pcl::PointXYZRGB>::Ptr colored_cloud = rgnGrw.getColoredCloud();
        pcl::PointCloud <pcl::PointXYZRGB>::Ptr non_classified_cloud(new pcl::PointCloud<pcl::PointXYZRGB>());
        pcl::PointCloud <pcl::PointXYZRGB>::Ptr classified_cloud(new pcl::PointCloud<pcl::PointXYZRGB>());

        if (!(*colored_cloud).empty() && (colored_cloud!=NULL))
        {
            for (auto& point : colored_cloud->points) {
                std::uint32_t rgb = *reinterpret_cast<int*>(&point.rgb);
                std::uint8_t r = (rgb >> 16) & 0x0000ff;
                std::uint8_t g = (rgb >> 8) & 0x0000ff;
                std::uint8_t b = (rgb) & 0x0000ff;

                if ((r != 0) & (g == 0) & (b == 0)) {
                    non_classified_cloud->push_back(point);
                }
                else {
                    classified_cloud->push_back(point);
                }
            }
        }
        
        int h = 9;
        if (!(*classified_cloud).empty()) {
            pcl::io::savePCDFile((outputFlPth + "\\" + outputFlNm + "_RG_RGB_C.pcd"), *classified_cloud);
        }
        if (!(*non_classified_cloud).empty()) {
            pcl::io::savePCDFile((outputFlPth + "\\" + outputFlNm + "_RG_RGB_NC.pcd"), *non_classified_cloud);
        }
        if (!(*colored_cloud).empty()) {
            pcl::io::savePCDFile((outputFlPth + "\\" + outputFlNm + "_RG_RGB.pcd"), *colored_cloud);
            return true;
        }
        else {
            return false;
        }
    }

    struct SOR_FILTERING_DATA {
        int NrK;
        double StdDevMult;
        bool Negative;
        bool KeepOrganized;
        float Value;

        bool BValue;
        bool BNrK;
        bool BStdDevMult;
    };

    bool PCLStatisticalOutlierRemoval(SOR_FILTERING_DATA data,
        std::string outputFlNm, std::string& outputFlPth, std::string cldFlPth) {

        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>());
        pcl::io::loadPCDFile<pcl::PointXYZ>(cldFlPth, *cloud);

        pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
        sor.setInputCloud(cloud);

        (data.BNrK) ? sor.setMeanK(data.NrK) : sor.setMeanK(10);
        (data.BStdDevMult) ? sor.setStddevMulThresh(data.StdDevMult) : sor.setStddevMulThresh(1.0);
        (data.KeepOrganized) ? sor.setKeepOrganized(data.KeepOrganized) : sor.setKeepOrganized(true);
        (data.Negative) ? sor.setNegative(data.Negative) : sor.setNegative(false);
        (data.BValue) ? sor.setUserFilterValue(data.Value) : true; // TODO: test the impact of "true" at this point

        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);
        sor.filter(*cloud_filtered);

        if (!(*cloud_filtered).empty()) {
            pcl::io::savePCDFile((outputFlPth + "\\" + outputFlNm + "_SOR.pcd"), *cloud_filtered);
            return true;
        }
        else {
            return false;
        }
    }

    struct SUPERVOXEL_CLUSTERING_DATA {
        float voxel_resolution;
        float seed_resolution;
        float color_importance;
        float spatial_importance;
        float normal_importance;

        bool use_single_cam_transform;
        bool use_supervoxel_refinement;

        bool Bvoxel_resolution;
        bool Bseed_resolution;
        bool Bcolor_importance;
        bool Bspatial_importance;
        bool Bnormal_importance;
    };

    struct LCCP_SEGMENTATION_DATA {
        float concavity_tolerance_threshold;
        float smoothness_threshold;

        std::uint32_t min_segment_size;
        unsigned int k_factor;

        bool use_sanity_criterion;

        bool Bconcavity_tolerance_threshold;
        bool Bsmoothness_threshold;
        bool Bmin_segment_size;
        bool Bk_factor;
    };

    struct CPC_SEGMENTATION_DATA {
        float min_cut_score;

        unsigned int max_cuts;
        unsigned int cutting_min_segments;
        unsigned int ransac_iterations;

        bool use_local_constrain;
        bool use_directed_cutting;
        bool use_clean_cutting;

        bool Bmin_cut_score;
        bool Bmax_cuts;
        bool Bcutting_min_segments;
        bool Bransac_iterations;

        SUPERVOXEL_CLUSTERING_DATA svData;
        LCCP_SEGMENTATION_DATA lccpData;

        bool Badd_label_field;
        bool Bsave_sv;
        
    };

    bool PCLCPCSegmentation(CPC_SEGMENTATION_DATA data,
        std::string outputFlNm, std::string& outputFlPth, std::string cldFlPth, std::string cldNrmlFlPth) {

        using PointT = pcl::PointXYZRGBA;  // The point type used for input

        /// Create variables needed for preparations
        std::string outputname = "keller0";

        pcl::PointCloud<PointT>::Ptr cloud(new pcl::PointCloud<PointT>());
        pcl::io::loadPCDFile<PointT>(cldFlPth, *cloud);

        pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>());
        pcl::io::loadPCDFile<pcl::Normal>(cldNrmlFlPth, *cloud_normals);

        /// check if the provided pcd file contains normals
        pcl::PCLPointCloud2 input_pointcloud2;
        pcl::fromPCLPointCloud2(input_pointcloud2, *cloud);


        /// Preparation of Input: Supervoxel Oversegmentation
        pcl::SupervoxelClustering<PointT> super(data.svData.voxel_resolution, data.svData.seed_resolution);
        super.setUseSingleCameraTransform(data.svData.use_single_cam_transform);
        super.setInputCloud(cloud);
        super.setNormalCloud(cloud_normals);
        super.setColorImportance(data.svData.color_importance);
        super.setSpatialImportance(data.svData.spatial_importance);
        super.setNormalImportance(data.svData.normal_importance);
        std::map<std::uint32_t, pcl::Supervoxel<PointT>::Ptr> supervoxel_clusters;
        super.extract(supervoxel_clusters);
        if (data.svData.use_supervoxel_refinement)
        {
            super.refineSupervoxels(2, supervoxel_clusters); // TODO: make 2 a selectable parameter
        }
        std::multimap<std::uint32_t, std::uint32_t>supervoxel_adjacency;
        super.getSupervoxelAdjacency(supervoxel_adjacency);

        /// Get the cloud of supervoxel centroid with normals and the colored cloud with supervoxel coloring (this is used for visulization)
        pcl::PointCloud<pcl::PointNormal>::Ptr sv_centroid_normal_cloud = pcl::SupervoxelClustering<PointT>::makeSupervoxelNormalCloud(supervoxel_clusters);



        //(data.BMinClusterSize) ? rgnGrw.setMinClusterSize(data.MinClusterSize) : rgnGrw.setMinClusterSize(5);


        /// LCCP preprocessing and CPC (CPC inherits from LCCP, thus it includes LCCP's functionality)
        pcl::CPCSegmentation<PointT> cpc;
        cpc.setInputSupervoxels(supervoxel_clusters, supervoxel_adjacency);
        cpc.setSmoothnessCheck(true, data.svData.voxel_resolution, data.svData.seed_resolution, data.lccpData.smoothness_threshold);
        cpc.setSanityCheck(data.lccpData.use_sanity_criterion);
        cpc.setConcavityToleranceThreshold(data.lccpData.concavity_tolerance_threshold);
        cpc.setKFactor(data.lccpData.k_factor);
        cpc.setMinSegmentSize(data.lccpData.min_segment_size);
        cpc.setCutting(data.max_cuts, data.cutting_min_segments, data.min_cut_score, data.use_local_constrain, data.use_directed_cutting, data.use_clean_cutting);
        cpc.setRANSACIterations(data.ransac_iterations);
        cpc.segment();






        /// Interpolation voxel cloud -> input cloud and relabeling
        pcl::PointCloud<pcl::PointXYZL>::Ptr sv_labeled_cloud = super.getLabeledCloud();
        pcl::PointCloud<pcl::PointXYZL>::Ptr cpc_labeled_cloud = sv_labeled_cloud->makeShared();
        cpc.relabelCloud(*cpc_labeled_cloud);
        //pcl::LCCPSegmentation<PointT>::SupervoxelAdjacencyList sv_adjacency_list;
        //cpc.getSVAdjacencyList(sv_adjacency_list);  // Needed for visualization

        if (cpc_labeled_cloud->size() == cloud->size()) {

            if (data.Badd_label_field)
            {
                if (pcl::getFieldIndex(input_pointcloud2, "label") >= 0) {
                    return false; // Input cloud already has a label field. It will be overwritten by the cpc segmentation output.
                }
                    
                pcl::PCLPointCloud2 output_label_cloud2, output_concat_cloud2;
                pcl::toPCLPointCloud2(*cpc_labeled_cloud, output_label_cloud2);
                pcl::concatenateFields(input_pointcloud2, output_label_cloud2, output_concat_cloud2);

                pcl::io::savePCDFile((outputFlPth + "\\" + outputFlNm + "_CPC_CONCAT.pcd"), output_concat_cloud2, Eigen::Vector4f::Zero(), Eigen::Quaternionf::Identity());

            }
            else {
                pcl::io::savePCDFile((outputFlPth + "\\" + outputFlNm + "_CPC.pcd"), *cpc_labeled_cloud);
            }
                
            if (data.Bsave_sv)
            {
                pcl::io::savePCDFile((outputFlPth + "\\" + outputFlNm + "_CPC_SV.pcd"), *sv_centroid_normal_cloud);
            }
            return true;

        }
        else {
            return false;
        }
    }

    void RenderUI()
    {
        DockSpace();

        ImGui::StyleColorsLight();
        ImGuiStyle& style = ImGui::GetStyle();
        style.GrabRounding = 0;
        style.FrameRounding = 0;
#ifdef _DEBUG
        ImGui::StyleColorsDark();
        style.GrabRounding = 0;
        style.FrameRounding = 0;
        //ImGui::ShowDemoWindow();      
#endif
        //ImGui::ShowDemoWindow();
        static ExampleAppLog log;
        log.Draw("Log", nullptr);

        const int modLim = 8;
        const char* modules[modLim] = { "Segmentation","Filtering","Boundary Estimation","Surface","Registration","Edge Detection","Transformation","Normal Estimation" };
        const char* categories[3] = { "[ INFO:]", "[ WARN:]", "[ERROR:]" };

        //static std::string inputCldPth = "E:\\data\\01_scan3d\\Segmente";
        //static std::string inputCldFle = "Gewoelbe";
        //static std::string inputCldFleDType = "pcd";

        //static std::string normalCldPth = "E:\\data\\01_scan3d\\Segmente";
        //static std::string normalCldFle = "Gewoelbe";
        //static std::string normalCldFleDType = "pcd";
        //
        //static std::string outputCldPth = "E:\\data\\01_scan3d\\Segmente";

        static std::string inputCldPth = "";
        static std::string inputCldFle = "";
        static std::string inputCldFleDType = "";

        static std::string normalCldPth = "";
        static std::string normalCldFle = "";
        static std::string normalCldFleDType = "";

        static std::string outputCldPth = "";

        static std::string initPath;
        static bool hasNrml = false;        // normals not calculated on init
        static bool actvteClclt = true;     // activate Calculate button on init
        static bool setScaleNormals = false;// scale normals for poisson surface rec
        POISSON_SURFACE_DATA psrData;
        O3D_POISSON_SURFACE_DATA o3dpsrData;
        BOUNDARY_ESTIMATION_DATA beData;
        SAC_SEGMENTATION_DATA sacData;
        SOR_FILTERING_DATA sorData;
        RG_SEGMENTATION_DATA rgData;
        SUPERVOXEL_CLUSTERING_DATA svData;
        LCCP_SEGMENTATION_DATA lccpData;
        CPC_SEGMENTATION_DATA cpcData;

        static int selected = 0;
        static int subselected = 0;
        static int selectedO3D = 0;
        static int subselectedO3D = 0;
        const int modLimO3D = 1;
        const char* modulesO3D[modLimO3D] = { "Surface" };

        ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize;

        ImGui::Begin("O3D",nullptr, window_flags);
        /*        {
            ImGui::BeginChild("left pane o3d", ImVec2(150, -ImGui::GetFrameHeightWithSpacing()), true);

            for (int i = 0; i < modLimO3D; i++)
            {
                if (ImGui::Selectable(modulesO3D[i], selectedO3D == i)) {
                    selectedO3D = i;
                    actvteClclt = switchSlctbl(selectedO3D,subselectedO3D);
                    hasNrml = false;
                }
            }

            ImGui::EndChild();

        }
        ImGui::SameLine();
        {
            ImGui::BeginGroup();
            ImGui::BeginChild("item view o3d", ImVec2(0, -ImGui::GetFrameHeightWithSpacing()), false, ImGuiWindowFlags_AlwaysAutoResize); // Leave room for 1 line below us
            {
                HelpMarker("Path to the PCD point cloud.");
                ImGui::SameLine();
                if (ImGui::Button(" Input Cloud"))
                    ImGuiFileDialog::Instance()->OpenDialog("ChooseFileDlgKey", "Choose File", ".pcd,.ply,.*", ".");
                if (ImGuiFileDialog::Instance()->Display("ChooseFileDlgKey"))
                //if (ImGuiFileDialog::Instance()->Display("ChooseFileDlgKey", ImGuiWindowFlags_NoMove))
                {
                    if (ImGuiFileDialog::Instance()->IsOk()) {
                        // filepath
                        inputCldPth = ImGuiFileDialog::Instance()->GetCurrentPath();
                        // filename
                        std::string inputCldFlPth = ImGuiFileDialog::Instance()->GetFilePathName();
                        int lenFull = inputCldFlPth.length();
                        int lenFullNoDataType = lenFull - 5; // .pcd\0
                        int lenDir = inputCldPth.length();
                        inputCldFle = inputCldFlPth.substr(lenDir + 1, (lenFullNoDataType - lenDir));
                    }
                    ImGuiFileDialog::Instance()->Close();
                }
                ImGui::SameLine();
                ImDrawList* draw_list = ImGui::GetWindowDrawList();
                if (!inputCldPth.empty())
                {
                    ImGui::TextWrapped(inputCldPth.c_str());
                    draw_list->AddRect(ImVec2(ImGui::GetItemRectMin().x - 3, ImGui::GetItemRectMin().y - 3),
                        ImVec2(ImGui::GetItemRectMax().x + 3, ImGui::GetItemRectMax().y + 3), IM_COL32(200, 200, 200, 255));
                }
                ImGui::SameLine(0, 20);
                if (!inputCldFle.empty())
                {
                    ImGui::TextWrapped((inputCldFle).c_str());
                    draw_list->AddRect(ImVec2(ImGui::GetItemRectMin().x - 3, ImGui::GetItemRectMin().y - 3),
                        ImVec2(ImGui::GetItemRectMax().x + 3, ImGui::GetItemRectMax().y + 3), IM_COL32(200, 200, 200, 255));
                }
            }
            ImGui::Spacing();
            {
                HelpMarker("Path to the resulting PCD point cloud.");
                ImGui::SameLine();
                if (ImGui::Button("Output Cloud"))
                    ImGuiFileDialog::Instance()->OpenDialog("ChooseDirDlgKey", "Choose Dir", nullptr, ".");
                if (ImGuiFileDialog::Instance()->Display("ChooseDirDlgKey"))
                //if (ImGuiFileDialog::Instance()->Display("ChooseDirDlgKey", ImGuiWindowFlags_NoMove))
                {
                    if (ImGuiFileDialog::Instance()->IsOk()) {
                        outputCldPth = ImGuiFileDialog::Instance()->GetCurrentPath();
                    }
                    ImGuiFileDialog::Instance()->Close();
                }
                ImGui::SameLine();
                if (!outputCldPth.empty())
                {
                    ImGui::TextWrapped(outputCldPth.c_str());
                    ImDrawList* draw_list = ImGui::GetWindowDrawList();
                    draw_list->AddRect(ImVec2(ImGui::GetItemRectMin().x - 3, ImGui::GetItemRectMin().y - 3),
                        ImVec2(ImGui::GetItemRectMax().x + 3, ImGui::GetItemRectMax().y + 3), IM_COL32(200, 200, 200, 255));
                }

            }

            ImGui::Spacing();
            ImGui::Spacing();
            if (ImGui::BeginTabBar("##TabsO3d", ImGuiTabBarFlags_None))
            {
                if (selectedO3D == 0) {
                    if (ImGui::BeginTabItem("Poisson Surface Recognition"))
                    {
                        static bool setlinear_fit = false;

                        static bool BsetDepth = true;
                        static bool BsetWidth = false;
                        static bool BsetScale = false;
                        static bool BsetNThreads = false;
                        static bool BsetVisRes = false;

                        static int setDepth = 8;
                        static float setWidth = 0.0f;
                        static float setScale = 1.1f;
                        static int setNThreads = -1;


                        {
                            ImGuiWindowFlags window_flags = ImGuiWindowFlags_HorizontalScrollbar;
                            ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                            ImGui::BeginChild("ChildL", ImVec2(ImGui::GetContentRegionAvail().x * 0.5f, 260), false, window_flags);

                            {
                                InputHelperInt(BsetDepth, setDepth, "##O3DPSRlsetDepth", "depth");
                                ImGui::SameLine(); HelpMarker("Maximum depth of the tree that will be used for surface reconstruction. Running at depth d corresponds to solving on a grid whose resolution is no larger than 2^d x 2^d x 2^d. Note that since the reconstructor adapts the octree to the sampling density, the specified reconstruction depth is only an upper bound.");
                            }

                            ImGui::Separator();

                            {
                                InputHelperFlt(BsetWidth, setWidth, "##O3DPSRlsetWidth", "width"),
                                    ImGui::SameLine(); HelpMarker("Specifies the target width of the finest level octree cells. This parameter is ignored if depth is specified.");
                            }


                            {
                                InputHelperFlt(BsetScale, setScale, "##O3DPSRlsetScale", "scale");
                                ImGui::SameLine(); HelpMarker("Specifies the ratio between the diameter of the cube used for reconstruction and the diameter of the samples' bounding cube.");
                            }

                            {
                                InputHelperInt(BsetNThreads, setNThreads, "##O3DPSRlsetNThreads", "# threads");
                                ImGui::SameLine(); HelpMarker("Number of threads used for reconstruction. Set to -1 to automatically determine it.");
                            }


                            ImGui::EndChild();
                            ImGui::PopStyleVar();
                        }
                        ImGui::SameLine();
                        {
                            ImGuiWindowFlags window_flags = ImGuiWindowFlags_HorizontalScrollbar;
                            ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                            ImGui::BeginChild("ChildR", ImVec2(0, 260), true, window_flags);

                            ImGui::Checkbox("linear fit", &setlinear_fit);
                            ImGui::SameLine(); HelpMarker("If true, the reconstructor use linear interpolation to estimate the positions of iso-vertices.");
                            ImGui::Checkbox("show visualization", &BsetVisRes);
                            ImGui::SameLine(); HelpMarker("If true, the open3d visualizer pops up after calculation and shows a preview of the mesh.");

                            ImGui::EndChild();
                            ImGui::PopStyleVar();
                        }

                        o3dpsrData.depth = setDepth;
                        o3dpsrData.width = setWidth;
                        o3dpsrData.scale = setScale;
                        o3dpsrData.linear_fit = setlinear_fit;
                        o3dpsrData.n_threads = setNThreads;
                        o3dpsrData.Bdepth = BsetDepth;
                        o3dpsrData.Bwidth = BsetWidth;
                        o3dpsrData.Bscale = BsetScale;
                        o3dpsrData.Bn_threads = BsetNThreads;
                        o3dpsrData.BvisRes = BsetVisRes;

                        //psrData._neompData = NORMAL_ESTIMATION_OMP_DATA(); // empty data --> returns 0 for NrThreads
                        ImGui::EndTabItem();
                    }
                }
                ImGui::EndTabBar();
            }

            ImGui::Spacing();
            ImGui::Spacing();

            if (ImGui::Button("Calculate")) {
                ImGui::OpenPopup("O3DCalculate");
            }

            ImGui::SetNextWindowPos(ImGui::GetMainViewport()->GetCenter(), ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
            if (ImGui::BeginPopupModal("O3DCalculate", NULL, ImGuiWindowFlags_AlwaysAutoResize))
            {
                ImGui::Text("The computation time can last up to several minutes.\n\n Continue?");
                ImGui::Separator();

                bool unused_open = true;
                static bool BdelNEOMPFl = false;
                static bool BuseStdNm = false;
                static std::string cldResFlNm = inputCldFle;
                if (ImGui::Button("Yes", ImVec2(120, 0))) {
                    ImGui::OpenPopup("O3DFile Setup");
                }

                ImGui::SetNextWindowPos(ImGui::GetMainViewport()->GetCenter(), ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
                if (ImGui::BeginPopupModal("O3DFile Setup", &unused_open))
                {
                    if (selectedO3D == 0) {

                        if (BuseStdNm)
                            ImGui::BeginDisabled();
                        ImGui::InputText("filename", &cldResFlNm);
                        if (BuseStdNm)
                            ImGui::EndDisabled();
                        ImGui::Spacing();
                        ImGui::Checkbox("use default filename", &BuseStdNm);
                        ImGui::SameLine();
                        ImGui::SameLine(); HelpMarker("Use default filename (i.e. name of PCD file).");
                        ImGui::Separator();
                        if (ImGui::Button("Ok")) {
                            // Poisson Surface Reconstruction
                            if (selectedO3D == 0)
                            {
                                if (BuseStdNm) {
                                    cldResFlNm = inputCldFle;
                                }
                                if (O3DPoissonSurfaceReconstruction(o3dpsrData, cldResFlNm, outputCldPth, (inputCldPth + "\\" + inputCldFle + "." + inputCldFleDType), " ")) {
                                    log.AddLog("[%05d] %s Poisson Surface Reconstruction done!\n", ImGui::GetFrameCount(), categories[0]);
                                    log.AddLog("[%05d] %s Mesh file stored at:\n\n%s\n\n", ImGui::GetFrameCount(), categories[0],
                                        (outputCldPth + "\\" + cldResFlNm + "_O3DPSR.ply").c_str());
                                }
                                else {
                                    log.AddLog("[%05d] %s Poisson Surface Reconstruction impossible!\n\n", ImGui::GetFrameCount(), categories[2]);
                                }
                            }
                            ImGui::ClosePopupToLevel(0, true);
                        }
                        ImGui::SameLine();
                        if (ImGui::Button("Cancel")) {
                            ImGui::ClosePopupToLevel(0, true);
                        }
                    }
                    ImGui::EndPopup();
                }
                ImGui::SetItemDefaultFocus();
                ImGui::SameLine();
                if (ImGui::Button("No", ImVec2(120, 0))) {
                    ImGui::CloseCurrentPopup();
                }
                ImGui::EndPopup();
            }

            ImGui::EndChild();
            ImGui::EndGroup();

            
        }
        */
        ImGui::End();
        ImGui::End();

        ImGui::Begin("PCL", nullptr, window_flags);
        {
            //ImGui::BeginChild("left pane", ImVec2(150, -ImGui::GetFrameHeightWithSpacing()), true);
            ImGuiWindowFlags window_flags = ImGuiWindowFlags_HorizontalScrollbar | ImGuiWindowFlags_AlwaysAutoResize;
            ImGui::BeginChild("left pane", ImVec2(ImGui::GetContentRegionAvail().x * 0.2, ImGui::GetContentRegionAvail().y), true, window_flags);

            for (int i = 0; i < modLim; i++)
            {
                if (ImGui::Selectable(modules[i], selected == i)) {
                    selected = i;
                    actvteClclt = switchSlctbl(selected, subselected);
                    hasNrml = false;
                    //log.AddLog("selected [%05d] subselected [%05d]\n\n", selected, subselected);
                }
            }

            ImGui::EndChild();

        }
        ImGui::SameLine();
        {
            ImGui::BeginGroup();

            {
                (inputCldPth == "") ? initPath = "." : initPath = inputCldPth + "\\"; // overwrite lookup path for dear im gui file dialog
                ImGui::BeginChild("input output pcl", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y * 0.2f), true, ImGuiWindowFlags_AlwaysAutoResize);
                {
                    ImGui::BeginChild("input pcl", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y * 0.5f), false, ImGuiWindowFlags_AlwaysAutoResize);
                    HelpMarker("Path to the PCD point cloud.");
                    ImGui::SameLine();
                    if (ImGui::Button(" Input Cloud"))
                        ImGuiFileDialog::Instance()->OpenDialog("ChooseFileDlgKey", "Choose File", ".pcd,.ply,.*", initPath);
                    if (ImGuiFileDialog::Instance()->Display("ChooseFileDlgKey", ImGuiWindowFlags_NoMove))
                    {
                        if (ImGuiFileDialog::Instance()->IsOk()) {
                            // filepath
                            inputCldPth = ImGuiFileDialog::Instance()->GetCurrentPath();
                            // filename
                            std::string inputCldFlPth = ImGuiFileDialog::Instance()->GetFilePathName();
                            int lenFull = inputCldFlPth.length();
                            int lenFullNoDataType = lenFull - 5; // .pcd\0
                            int lenFullWDataType = lenFull - 1; // .pcd\0
                            int lenDir = inputCldPth.length();
                            inputCldFle = inputCldFlPth.substr(lenDir + 1, (lenFullNoDataType - lenDir));
                            inputCldFleDType = inputCldFlPth.substr(lenFullNoDataType + 2, 3);
                        }
                        ImGuiFileDialog::Instance()->Close();
                    }
                    ImGui::SameLine();
                    ImDrawList* draw_list = ImGui::GetWindowDrawList();
                    if (!inputCldPth.empty())
                    {
                        ImGui::TextWrapped(inputCldPth.c_str());
                        draw_list->AddRect(ImVec2(ImGui::GetItemRectMin().x - 3, ImGui::GetItemRectMin().y - 3),
                            ImVec2(ImGui::GetItemRectMax().x + 3, ImGui::GetItemRectMax().y + 3), IM_COL32(200, 200, 200, 255));
                    }
                    ImGui::SameLine(0, 20);
                    if (!inputCldFle.empty())
                    {
                        ImGui::TextWrapped((inputCldFle).c_str());
                        draw_list->AddRect(ImVec2(ImGui::GetItemRectMin().x - 3, ImGui::GetItemRectMin().y - 3),
                            ImVec2(ImGui::GetItemRectMax().x + 3, ImGui::GetItemRectMax().y + 3), IM_COL32(200, 200, 200, 255));
                    }
                    ImGui::EndChild();
                }
                ImGui::Spacing();
                {
                    ImGui::BeginChild("output pcl", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), false, ImGuiWindowFlags_AlwaysAutoResize);
                    HelpMarker("Path to the resulting PCD point cloud.");
                    ImGui::SameLine();
                    if (ImGui::Button("Output Cloud"))
                        ImGuiFileDialog::Instance()->OpenDialog("ChooseDirDlgKey", "Choose Dir", nullptr, initPath);
                    if (ImGuiFileDialog::Instance()->Display("ChooseDirDlgKey", ImGuiWindowFlags_NoMove))
                    {
                        if (ImGuiFileDialog::Instance()->IsOk()) {
                            outputCldPth = ImGuiFileDialog::Instance()->GetCurrentPath();
                        }
                        ImGuiFileDialog::Instance()->Close();
                    }
                    ImGui::SameLine();
                    if (!outputCldPth.empty())
                    {
                        ImGui::TextWrapped(outputCldPth.c_str());
                        ImDrawList* draw_list = ImGui::GetWindowDrawList();
                        draw_list->AddRect(ImVec2(ImGui::GetItemRectMin().x - 3, ImGui::GetItemRectMin().y - 3),
                            ImVec2(ImGui::GetItemRectMax().x + 3, ImGui::GetItemRectMax().y + 3), IM_COL32(200, 200, 200, 255));
                    }
                    ImGui::EndChild();
                }
                ImGui::EndChild();
            }

            {
                //ImGui::BeginChild("item view", ImVec2(0, -ImGui::GetFrameHeightWithSpacing()), false, ImGuiWindowFlags_AlwaysAutoResize); // Leave room for 1 line below us
                ImGui::BeginChild("item view", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y * 0.75f), true); // Leave room for 1 line below us
                //ImGui::BeginChild("item view", ImVec2(0, 0), false, ImGuiWindowFlags_AlwaysAutoResize); // Leave room for 1 line below us
                if (ImGui::BeginTabBar("##Tabs", ImGuiTabBarFlags_None))
                {
                    // Segmentation
                    if (selected == 0) {
                        if (ImGui::BeginTabItem("SAC Segmentation"))
                        {
                            subselected = 0;
                            actvteClclt = switchSlctbl(selected, subselected);

                            const char* itemsMethod[] = { "SAC_RANSAC", "SAC_LMEDS", "SAC_MSAC", "SAC_RRANSAC", "SAC_RMSAC", "SAC_MLESAC", "SAC_PROSAC" };
                            static int item_current_method = 0;
                            const char* itemsModel[] = {
                                        "SACMODEL_PLANE",
                                        "SACMODEL_LINE",
                                        "SACMODEL_CIRCLE2D",
                                        "SACMODEL_CIRCLE3D",
                                        "SACMODEL_SPHERE",
                                        "SACMODEL_CYLINDER",
                                        "SACMODEL_CONE",
                                        "SACMODEL_TORUS",
                                        "SACMODEL_PARALLEL_LINE",
                                        "SACMODEL_PERPENDICULAR_PLANE",
                                        "SACMODEL_PARALLEL_LINES",
                                        "SACMODEL_NORMAL_PLANE",
                                        "SACMODEL_NORMAL_SPHERE",
                                        "SACMODEL_REGISTRATION",
                                        "SACMODEL_REGISTRATION_2D",
                                        "SACMODEL_PARALLEL_PLANE",
                                        "SACMODEL_NORMAL_PARALLEL_PLANE",
                                        "SACMODEL_STICK"
                            };
                            static int item_current_model = 0;

                            static int setMaxIter = 50;
                            static double setThreshold = 1.0;
                            static double setProb = 1.0;
                            static int setNrThreads = 2;
                            static double setRadiusLimitMin = 1.0;
                            static double setRadiusLimitMax = 10.0;
                            double setSamplesMaxDist = 1.0; // TODO non static because not implemented in function yet
                            static double setEpsilonAngle = 1.0;

                            static bool setOptimizeCoefficients = false;
                            static bool setNegativeInd = false;

                            static bool BsetMaxIter = false;
                            static bool BsetThreshold = false;
                            static bool BsetProb = false;
                            static bool BsetNrThreads = false;
                            static bool BsetRadiusLimit = false;
                            static bool BsetSamplesMaxDist = false;
                            static bool BsetEpsilonAngle = false;

                            {
                                ImGuiWindowFlags window_flags = ImGuiWindowFlags_HorizontalScrollbar;
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildL", ImVec2(ImGui::GetContentRegionAvail().x * 0.5f, ImGui::GetContentRegionAvail().y), false, window_flags);

                                ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.25f);
                                ImGui::Combo("Method type ", &item_current_method, itemsMethod, IM_ARRAYSIZE(itemsMethod));
                                ImGui::SameLine(); HelpMarker("The type of sample consensus method to use (user given parameter).");

                                ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.25f);
                                ImGui::Combo("Model type", &item_current_model, itemsModel, IM_ARRAYSIZE(itemsModel));
                                ImGui::SameLine(); HelpMarker("The type of model to use (user given parameter).");

                                {
                                    InputHelperInt(BsetMaxIter, setMaxIter, "##SAClsetMaxIter", "Max. Iterations");
                                    ImGui::SameLine(); HelpMarker("Set the maximum number of iterations before giving up.");
                                }
                                {
                                    InputHelperDbl(BsetThreshold, setThreshold, "##SAClsetThreshold", "Threshold");
                                    ImGui::SameLine(); HelpMarker("Distance to the model threshold (user given parameter).");
                                }
                                {
                                    InputHelperDbl(BsetProb, setProb, "##SAClsetProb", "Probability");
                                    ImGui::SameLine(); HelpMarker("Set the probability of choosing at least one sample free from outliers.");
                                }
                                {
                                    InputHelperInt(BsetNrThreads, setNrThreads, "##SAClsetNrThreads", "# threads");
                                    ImGui::SameLine(); HelpMarker("Set the number of threads to use or turn off parallelization.");
                                }
                                {
                                    InputHelperDbl(BsetRadiusLimit, setRadiusLimitMin, "##SAClsetRadiusLimitMin", "Radius limit min");
                                    ImGui::SameLine(); HelpMarker("Set the minimum allowable radius limits for the model (applicable to models that estimate a radius)");
                                }
                                {
                                    InputHelperDbl(BsetRadiusLimit, setRadiusLimitMax, "##SAClsetRadiusLimitMax", "Radius limit max");
                                    ImGui::SameLine(); HelpMarker("Set the maximum allowable radius limits for the model (applicable to models that estimate a radius)");
                                }
                                {
                                    InputHelperDbl(BsetSamplesMaxDist, setSamplesMaxDist, "##SAClsetSamplesMaxDist", "Samples max dist");
                                    ImGui::SameLine(); HelpMarker("CURRENTLY OUT OF ORDER!\n\nSet the maximum distance allowed when drawing random samples.");
                                }
                                {
                                    InputHelperDbl(BsetEpsilonAngle, setEpsilonAngle, "##SAClsetEpsilonAngle", "Epsilon angle");
                                    ImGui::SameLine(); HelpMarker("Set the angle epsilon (delta) threshold in rad.");
                                }

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }
                            ImGui::SameLine();
                            {
                                ImGuiWindowFlags window_flags = ImGuiWindowFlags_HorizontalScrollbar;
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                //ImGui::BeginChild("ChildR", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), true, window_flags);
                                ImGui::BeginChild("ChildR", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), false, window_flags);

                                ImGui::Checkbox("Optimize coefficients", &setOptimizeCoefficients);
                                ImGui::SameLine(); HelpMarker("Set to true if a coefficient refinement is required.");
                                ImGui::Checkbox("Negative indices", &setNegativeInd);
                                ImGui::SameLine(); HelpMarker("Set whether the regular conditions for points filtering should apply, or the inverted conditions.");

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }

                            sacData.SACMethod = item_current_method;
                            sacData.SACModel = item_current_model;

                            sacData.MaxIter = setMaxIter;
                            sacData.Threshold = setThreshold;
                            sacData.Prob = setProb;
                            sacData.NrThreads = setNrThreads;
                            sacData.RadiusLimitMin = setRadiusLimitMin;
                            sacData.RadiusLimitMax = setRadiusLimitMax;
                            sacData.SamplesMaxDist = setSamplesMaxDist;
                            sacData.EpsilonAngle = setEpsilonAngle;
                            sacData.NegativeInd = setNegativeInd;

                            sacData.BMaxIter = BsetMaxIter;
                            sacData.BThreshold = BsetThreshold;
                            sacData.BProb = BsetProb;
                            sacData.BNrThreads = BsetNrThreads;
                            sacData.BRadiusLimit = BsetRadiusLimit;
                            sacData.BSamplesMaxDist = BsetSamplesMaxDist;
                            sacData.BEpsilonAngle = BsetEpsilonAngle;

                            ImGui::EndTabItem();
                        }
                        if (ImGui::BeginTabItem("Region Growing"))
                        {

                            subselected = 1;
                            actvteClclt = switchSlctbl(selected, subselected);

                            static int setMinClusterSize = 1;
                            static int setMaxClusterSize = 10;
                            static int setNumNeighbors;
                            static float setSmoothThreshold;
                            static float setCurvatureThreshold;
                            static float setResidualThreshold;

                            static bool BsetSmoothModeFlag;
                            static bool BsetCurvatureTestFlag;
                            static bool BsetResidualTestFlag;
                            static bool BsetNumNeighbors;
                            static bool BsetMinClusterSize = true;
                            static bool BsetMaxClusterSize = true;

                            {
                                ImGuiWindowFlags window_flags = ImGuiWindowFlags_HorizontalScrollbar;
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildL", ImVec2(ImGui::GetContentRegionAvail().x * 0.5f, ImGui::GetContentRegionAvail().y), false, window_flags);

                                {
                                    InputHelperInt(BsetMinClusterSize, setMinClusterSize, "##RGsetMinClusterSize", "Min cluster size");
                                    ImGui::SameLine(); HelpMarker("Set the minimum number of points that a cluster needs to contain in order to be considered valid.");
                                }
                                {
                                    InputHelperInt(BsetMaxClusterSize, setMaxClusterSize, "##RGsetMaxClusterSize", "Max cluster size");
                                    ImGui::SameLine(); HelpMarker("Set the maximum number of points that a cluster needs to contain in order to be considered valid.");
                                }
                                {
                                    InputHelperInt(BsetNumNeighbors, setNumNeighbors, "##RGsetNumNeighbors", "Num neighbors");
                                    ImGui::SameLine(); HelpMarker("Allows to set the number of neighbours.");
                                }
                                {
                                    InputHelperFltSld(BsetSmoothModeFlag, setSmoothThreshold, "##RGsetSmoothThreshold", "smooth threshold", M_PI);
                                    ImGui::SameLine(); HelpMarker("Allows to set smoothness threshold used for testing the points.");
                                }
                                {
                                    InputHelperFlt(BsetCurvatureTestFlag, setCurvatureThreshold, "##RGsetCurvatureThreshold", "curvature threshold");
                                    ImGui::SameLine(); HelpMarker("Allows to set curvature threshold used for testing the points.");
                                }
                                {
                                    InputHelperFlt(BsetResidualTestFlag, setResidualThreshold, "##RGsetResidualThreshold", "residual threshold");
                                    ImGui::SameLine(); HelpMarker("Allows to set residual threshold used for testing the points.");
                                }

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }
                            ImGui::SameLine();
                            {
                                ImGuiWindowFlags window_flags = ImGuiWindowFlags_HorizontalScrollbar;
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildR", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), false, window_flags);

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }

                            rgData.BMinClusterSize = BsetMinClusterSize;
                            rgData.BMaxClusterSize = BsetMaxClusterSize;
                            rgData.BCurvatureTestFlag = BsetCurvatureTestFlag;
                            rgData.BNumNeighbors = BsetNumNeighbors;
                            rgData.BResidualTestFlag = BsetResidualTestFlag;
                            rgData.BSmoothModeFlag = BsetSmoothModeFlag;
                            rgData.CurvatureThreshold = setCurvatureThreshold;
                            rgData.MaxClusterSize = setMaxClusterSize;
                            rgData.MinClusterSize = setMinClusterSize;
                            rgData.NumNeighbors = setNumNeighbors;
                            rgData.ResidualThreshold = setResidualThreshold;
                            rgData.SmoothThreshold = setSmoothThreshold;

                            ImGui::EndTabItem();
                        }
                        if (ImGui::BeginTabItem("CPC"))
                        {
                            subselected = 2;
                            actvteClclt = switchSlctbl(selected, subselected);

                            static float setMinCutScore;

                            static int setMaxCuts;
                            static int setCuttingMinSegments;
                            static int setRANSACIterations;

                            static bool setUseLocalConstrain;
                            static bool setUseDirectedCutting;
                            static bool setUseCleanCutting;

                            static bool BsetMinCutScore;
                            static bool BsetMaxCuts;
                            static bool BsetCuttingMinSegments;
                            static bool BsetRANSACIterations;

                            // SV Data

                            static float setVoxelResolution;
                            static float setSeedResolution;
                            static float setColorImportance;
                            static float setSpatialImportance;
                            static float setNormalImportance;

                            static bool setUseSingleCamTransform;
                            static bool setUseSuperVoxelRefinement;

                            static bool BsetVoxelResolution;
                            static bool BsetSeedResolution;
                            static bool BsetColorImportance;
                            static bool BsetSpatialImportance;
                            static bool BsetNormalImportance;

                            // LCCP Data

                            static float setConcavityToleranceThreshold;
                            static float setSmoothnessThreshold;

                            static int setMinSegmentSize;
                            static int setKFactor;

                            static bool setUseSanityCriterion;
                            static bool setUseExtendedConvexity;

                            static bool BsetConcavityToleranceThreshold;
                            static bool BsetSmoothnessThreshold;
                            static bool BsetMinSegmentSize;
                            static bool BsetKFactor;


                            {
                                ImGuiWindowFlags window_flags = ImGuiWindowFlags_HorizontalScrollbar;
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildL", ImVec2(ImGui::GetContentRegionAvail().x * 0.5f, ImGui::GetContentRegionAvail().y), false, window_flags);

                                {
                                    InputHelperFlt(BsetMinCutScore, setMinCutScore, "##CPCBsetMinCutScore", "set min cut score");
                                    ImGui::SameLine(); HelpMarker("Minimum score a proposed cut has to achieve for being performed.");
                                }
                                {
                                    InputHelperInt(BsetCuttingMinSegments, setCuttingMinSegments, "##CPCsetCuttingMinSegments", "set cutting min segments");
                                    ImGui::SameLine(); HelpMarker("Minimum segment size for cutting");
                                }
                                {
                                    InputHelperInt(BsetMaxCuts, setMaxCuts, "##CPCsetMaxCuts", "set max cuts");
                                    ImGui::SameLine(); HelpMarker("Maximum number of cuts.");
                                }
                                {
                                    InputHelperInt(BsetRANSACIterations, setRANSACIterations, "##CPCsetRANSACIterations", "set RANSAC iterations");
                                    ImGui::SameLine(); HelpMarker("The number of iterations.");
                                }
                                // SV Data
                                ImGui::Separator();
                                ImGui::Text("Supervoxel Setup");
                                {
                                    InputHelperFlt(BsetVoxelResolution, setVoxelResolution, "##CPCsetVoxelResolution", "set voxel resolution");
                                    ImGui::SameLine(); HelpMarker("Set the resolution of the octree voxels.");
                                }
                                {
                                    InputHelperFlt(BsetSeedResolution, setSeedResolution, "##CPCsetSeedResolution", "set seed resolution");
                                    ImGui::SameLine(); HelpMarker("Set the resolution of the octree seed voxels.");
                                }
                                {
                                    InputHelperFlt(BsetColorImportance, setColorImportance, "##CPCsetColorImportance", "set color importance");
                                    ImGui::SameLine(); HelpMarker("Set the importance of color for supervoxels.");
                                }
                                {
                                    InputHelperFlt(BsetSpatialImportance, setSpatialImportance, "##CPCsetSpatialImportance", "set spatial importance");
                                    ImGui::SameLine(); HelpMarker("Set the importance of spatial distance for supervoxels.");
                                }
                                {
                                    InputHelperFlt(BsetNormalImportance, setNormalImportance, "##CPCsetNormalImportance", "set normal importance");
                                    ImGui::SameLine(); HelpMarker("Set the importance of scalar normal product for supervoxels.");
                                }
                                // LCCP Data
                                ImGui::Separator();
                                ImGui::Text("LCCP Setup");
                                {
                                    InputHelperFlt(BsetConcavityToleranceThreshold, setConcavityToleranceThreshold, "##CPCsetConcavityToleranceThreshold", "set concavity tolerance threshold");
                                    ImGui::SameLine(); HelpMarker("Set normal threshold in degree.");
                                }
                                {
                                    InputHelperFlt(BsetSmoothnessThreshold, setSmoothnessThreshold, "##CPCsetSmoothnessThreshold", "set smootheness threshold");
                                    ImGui::SameLine(); HelpMarker("Threshold (/fudging factor) for smoothness constraint according to the following formula. \n\n Determines if a smoothness check is done during segmentation, trying to invalidate edges of non-smooth connected edges (steps). \n Two supervoxels are unsmooth if their plane - to - plane distance\n\n DIST > (expected_distance + smoothness_threshold_ * voxel_resolution_). For parallel supervoxels, the expected_distance is zero.");
                                }
                                {
                                    InputHelperInt(BsetMinSegmentSize, setMinSegmentSize, "##CPCsetMinSegmentSize", "set min segment size");
                                    ImGui::SameLine(); HelpMarker("Set the value used in mergeSmallSegments. ??");
                                }
                                {
                                    InputHelperInt(BsetKFactor, setKFactor, "##CPCsetKFactor", "set k factor");
                                    ImGui::SameLine(); HelpMarker("Set the value used for k convexity.\n\n For k > 0 convex connections between p_i and p_j require k common neighbors of these patches that have a convex connection to both.");
                                }

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }
                            ImGui::SameLine();
                            {
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildR", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), false, window_flags);

                                ImGui::Checkbox("Use local constrain", &setUseLocalConstrain);
                                ImGui::SameLine(); HelpMarker("Decide if cuts are locally constrained (i.e. not globally).");
                                ImGui::Checkbox("Use directed cutting", &setUseDirectedCutting);
                                ImGui::SameLine(); HelpMarker("	Decide if cuts perpendicular to the edge-direction are preferred.");
                                ImGui::Checkbox("Use clean cutting", &setUseCleanCutting);
                                ImGui::SameLine(); HelpMarker("Decide if only edges with supervoxels on opposite sides of the plane (clean) are getting cutted or all edges within the seed_resolution_ distance to the plane (not clean).");

                                // SV Data
                                ImGui::Separator();
                                ImGui::Text("Supervoxel Setup");
                                ImGui::Checkbox("Use single cam transform", &setUseSingleCamTransform);
                                ImGui::SameLine(); HelpMarker("By default it will be used for organized clouds, but not for unorganized - this parameter will override that behavior The single camera transform scales bin size so that it increases exponentially with depth (z dimension). This is done to account for the decreasing point density found with depth when using an RGB-D camera. Without the transform, beyond a certain depth adjacency of voxels breaks down unless the voxel size is set to a large value. Using the transform allows preserving detail up close, while allowing adjacency at distance. The specific transform used here is: x /= z; y /= z; z = ln(z); This transform is applied when calculating the octree bins in OctreePointCloudAdjacency.");
                                ImGui::Checkbox("Use supervoxel refinement", &setUseSuperVoxelRefinement);
                                ImGui::SameLine(); HelpMarker("This method refines the calculated supervoxels - may only be called after extract. Fixed to 2 itarations in code.");

                                // LCCP Data
                                ImGui::Separator();
                                ImGui::Text("LCCP Setup");

                                static bool setUseSanityCriterion;

                                ImGui::Checkbox("Use sanity criterion", &setUseSanityCriterion);
                                ImGui::SameLine(); HelpMarker("Determines if the sanity criterion to invalidate singular connected patches should be used.");

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }

                            cpcData.min_cut_score = setMinCutScore;
                            cpcData.max_cuts = setMaxCuts;
                            cpcData.cutting_min_segments = setCuttingMinSegments;
                            cpcData.ransac_iterations = setRANSACIterations;

                            cpcData.use_clean_cutting = setUseCleanCutting;
                            cpcData.use_directed_cutting = setUseDirectedCutting;
                            cpcData.use_local_constrain = setUseLocalConstrain;

                            cpcData.Bmin_cut_score = BsetMinCutScore;
                            cpcData.Bmax_cuts = BsetMaxCuts;
                            cpcData.Bcutting_min_segments = BsetCuttingMinSegments;
                            cpcData.Bransac_iterations = BsetRANSACIterations;



                            // TODO
                            // - Variablen verbinden
                            // - Funktionsaufruf einbauen
                            // - zwei boolean in Calculate f�r die extra Dateien - da auch gleich noch die anderen f�r Region Growing (NC unc C)












                            ImGui::EndTabItem();
                        }
                        if (ImGui::BeginTabItem("..."))
                        {
                            {
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildL", ImVec2(ImGui::GetContentRegionAvail().x * 0.5f, ImGui::GetContentRegionAvail().y), false, true);

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }
                            ImGui::SameLine();
                            {
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildR", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), false, true);

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }

                            ImGui::EndTabItem();
                        }
                    }
                    // Filtering
                    if (selected == 1) {
                        if (ImGui::BeginTabItem("Statistical Outlier Removal"))
                        {
                            subselected = 0;

                            static int setNrK = 50;
                            static double setStdDevMult = 1.0;
                            static float setValue;

                            static bool setKeepOrganized = false;
                            static bool setNegative = false;

                            static bool BsetNrK = true;
                            static bool BsetStdDevMult = true;
                            static bool BsetValue = false;

                            {
                                ImGuiWindowFlags window_flags = ImGuiWindowFlags_HorizontalScrollbar;
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildL", ImVec2(ImGui::GetContentRegionAvail().x * 0.5f, ImGui::GetContentRegionAvail().y), false, window_flags);

                                {
                                    InputHelperInt(BsetNrK, setNrK, "##SORsetNrK", "Nr K");
                                    ImGui::SameLine(); HelpMarker("The number of points to use for mean distance estimation.");
                                }
                                {
                                    InputHelperDbl(BsetStdDevMult, setStdDevMult, "##SORsetStdDevMult", "Stddev multiplier");
                                    ImGui::SameLine(); HelpMarker("Standard deviations threshold (i.e., for value 3 points outside of M +- 3*s be marked as outliers).");
                                }
                                {
                                    InputHelperFlt(BsetValue, setValue, "##SORsetValue", "Custom value");
                                    ImGui::SameLine(); HelpMarker("Set whether the filtered points should be kept and set to the value given through setUserFilterValue (default: NaN), or removed from the PointCloud, thus potentially breaking its organized structure.. Provide a value that the filtered points should be set to instead of removing them.");
                                }

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }
                            ImGui::SameLine();
                            {
                                ImGuiWindowFlags window_flags = ImGuiWindowFlags_HorizontalScrollbar;
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildR", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), false, window_flags);

                                ImGui::Checkbox("Keep organized", &setKeepOrganized);
                                ImGui::SameLine(); HelpMarker("");
                                ImGui::Checkbox("Negative", &setNegative);
                                ImGui::SameLine(); HelpMarker("Set whether the regular conditions for points filtering should apply, or the inverted conditions.");

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }

                            sorData.NrK = setNrK;
                            sorData.StdDevMult = setStdDevMult;
                            sorData.Value = setValue;

                            sorData.KeepOrganized = setKeepOrganized;
                            sorData.Negative = setNegative;

                            sorData.BValue = BsetValue;
                            sorData.BNrK = BsetNrK;
                            sorData.BStdDevMult = BsetStdDevMult;

                            ImGui::EndTabItem();
                        }
                        if (ImGui::BeginTabItem("..."))
                        {
                            {
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildL", ImVec2(ImGui::GetContentRegionAvail().x * 0.5f, ImGui::GetContentRegionAvail().y), false, true);

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }
                            ImGui::SameLine();
                            {
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildR", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), false, true);

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }

                            ImGui::EndTabItem();
                        }
                    }
                    // Boundary Estimation
                    if (selected == 2) {
                        if (ImGui::BeginTabItem("Boundary Estimation"))
                        {
                            static double setRadiusSearch = 0.5;
                            static bool BsetRadiusSearch = true;

                            {
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildL", ImVec2(ImGui::GetContentRegionAvail().x * 0.5f, ImGui::GetContentRegionAvail().y), false, true);

                                InputHelperDbl(BsetRadiusSearch, setRadiusSearch, "##BEsetRadiusSearch", "Radius search");
                                ImGui::SameLine(); HelpMarker("Set the sphere radius that is to be used for determining the nearest neighbors used for the feature estimation.");

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }
                            ImGui::SameLine();
                            {
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildR", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), false, true);

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }

                            beData.RadiusSearch = setRadiusSearch;

                            //psrData._neompData = NORMAL_ESTIMATION_OMP_DATA(); // empty data --> returns 0 for NrThreads
                            ImGui::EndTabItem();
                        }
                        if (ImGui::BeginTabItem("..."))
                        {
                            {
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildL", ImVec2(ImGui::GetContentRegionAvail().x * 0.5f, ImGui::GetContentRegionAvail().y), false, true);

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }
                            ImGui::SameLine();
                            {
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildR", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), false, true);

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }

                            ImGui::EndTabItem();
                        }
                    }
                    // Poisson Surface Recognition
                    if (selected == 3) {
                        if (ImGui::BeginTabItem("Poisson Surface Recognition"))
                        {
                            static bool BsetNrThreads = true;
                            static bool BsetPoissonDepth = true;
                            static bool BsetKSearch = false;
                            static bool BsetPoissonMinDepth = false;
                            static bool BsetPoissonPointWeight = false;
                            static bool BsetPoissonScale = false;
                            static bool BsetPoissonSolverDividede = false;
                            static bool BsetPoissonIsoDivide = false;
                            static bool BsetPoissonSamplesPerNode = false;
                            static bool BsetPoissonDegree = false;
                            static int setNrThreads = 2;
                            static int setPoissonDepth = 5;
                            static int setPoissonMinDepth = 2;
                            static float setPoissonPointWeight = 1.0;
                            static float setPoissonScale = 1.0;
                            static int setPoissonSolverDividede = 7;
                            static int setPoissonIsoDivide = 7;
                            static float setPoissonSamplesPerNode = 3.0;
                            static int setPoissonDegree = 2;
                            static bool setPoissonConfidence = false;
                            static bool setPoissonOutputPolygons = false;
                            static bool setPoissonManifold = false;



                            {
                                ImGuiWindowFlags window_flags = ImGuiWindowFlags_HorizontalScrollbar;
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildL", ImVec2(ImGui::GetContentRegionAvail().x * 0.5f, ImGui::GetContentRegionAvail().y), false, window_flags);

                                {
                                    InputHelperInt(BsetNrThreads, setNrThreads, "##PSRlsetNrThreads", "# threads");
                                    ImGui::SameLine(); HelpMarker("Set the number of threads to use. Crashes most times when != 2. Choose wisely ...");
                                }
                                {
                                    InputHelperInt(BsetPoissonDepth, setPoissonDepth, "##PSRlsetPoissonDepth", "Poisson depth"),
                                        ImGui::SameLine(); HelpMarker("Set the maximum depth of the tree that will be used for surface reconstruction.");
                                }

                                ImGui::Separator();

                                {
                                    InputHelperInt(BsetPoissonMinDepth, setPoissonMinDepth, "##PSRlsetPoissonMinDepth", "Poisson min depth"),
                                        ImGui::SameLine(); HelpMarker("");
                                }


                                {
                                    InputHelperFlt(BsetPoissonPointWeight, setPoissonPointWeight, "##PSRlsetPoissonPointWeight", "Poisson point weight");
                                    ImGui::SameLine(); HelpMarker("");
                                }

                                {
                                    InputHelperFlt(BsetPoissonScale, setPoissonScale, "##PSRlsetPoissonScale", "Scale");
                                    ImGui::SameLine(); HelpMarker("Set the ratio between the diameter of the cube used for reconstruction and the diameter of the samples' bounding cube.");
                                }

                                {
                                    InputHelperInt(BsetPoissonSolverDividede, setPoissonSolverDividede, "##PSRlsetPoissonSolverDividede", "Solver divide");
                                    ImGui::SameLine(); HelpMarker("Set the the depth at which a block Gauss-Seidel solver is used to solve the Laplacian equation Using this parameter helps reduce the memory overhead at the cost of a small increase in reconstruction time. (In practice, we have found that for reconstructions of depth 9 or higher a subdivide depth of 7 or 8 can greatly reduce the memory usage.)");

                                }

                                {
                                    InputHelperInt(BsetPoissonIsoDivide, setPoissonIsoDivide, "##PSRlsetPoissonIsoDivide", "Iso Divide");
                                    ImGui::SameLine(); HelpMarker("Set the depth at which a block iso-surface extractor should be used to extract the iso-surface Using this parameter helps reduce the memory overhead at the cost of a small increase in extraction time. (In practice, we have found that for reconstructions of depth 9 or higher a subdivide depth of 7 or 8 can greatly reduce the memory usage.)");

                                }

                                {
                                    InputHelperFlt(BsetPoissonSamplesPerNode, setPoissonSamplesPerNode, "##PSRlsetPoissonSamplesPerNode", "Min # samples per node");
                                    ImGui::SameLine(); HelpMarker("Set the minimum number of sample points that should fall within an octree node as the octree construction is adapted to sampling density. For noise - free samples, small values in the range[1.0 - 5.0] can be used.For more noisy samples, larger values in the range[15.0 - 20.0] may be needed to provide a smoother, noise - reduced, reconstruction.");
                                }

                                {
                                    InputHelperInt(BsetPoissonDegree, setPoissonDegree, "##PSRlsetPoissonDegree", "Degree"),
                                        ImGui::SameLine(); HelpMarker("Set the degree parameter.");
                                }

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }
                            ImGui::SameLine();
                            {
                                ImGuiWindowFlags window_flags = ImGuiWindowFlags_HorizontalScrollbar;
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildR", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), false, window_flags);

                                ImGui::Checkbox("Set confidence flag", &setPoissonConfidence);
                                ImGui::SameLine(); HelpMarker("Enabling this flag tells the reconstructor to use the size of the normals as confidence information. When the flag is not enabled, all normals are normalized to have unit - length prior to reconstruction.");
                                if (!setPoissonConfidence) {
                                    ImGui::BeginDisabled();
                                    setScaleNormals = false;
                                }
                                ImGui::Checkbox("Scale normals with curvature", &setScaleNormals);
                                if (!setPoissonConfidence) {
                                    ImGui::EndDisabled();
                                }
                                ImGui::SameLine(); HelpMarker("Scale normals with corresponding point curvature.");
                                ImGui::Separator();
                                ImGui::Checkbox("Set polygon flag", &setPoissonOutputPolygons);
                                ImGui::SameLine(); HelpMarker("Enabling this flag tells the reconstructor to output a polygon mesh (rather than triangulating the results of Marching Cubes).");
                                ImGui::Checkbox("Set manifold flag", &setPoissonManifold);
                                ImGui::SameLine(); HelpMarker("Enabling this flag tells the reconstructor to add the polygon barycenter when triangulating polygons with more than three vertices.");


                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }

                            //psrData.inputCldFlPth = inputCldFlPth;
                            //psrData.inputCldPth = inputCldPth;
                            //psrData.outputCldPth = outputCldPth;

                            psrData.PoissonDepth = setPoissonDepth;
                            psrData.PoissonThreads = setNrThreads;
                            psrData.PoissonMinDepth = setPoissonMinDepth;
                            psrData.PoissonPointWeight = setPoissonPointWeight;
                            psrData.PoissonScale = setPoissonScale;
                            psrData.PoissonSolverDividede = setPoissonSolverDividede;
                            psrData.PoissonIsoDivide = setPoissonIsoDivide;
                            psrData.PoissonSamplesPerNode = setPoissonSamplesPerNode;
                            psrData.PoissonDegree = setPoissonDegree;

                            psrData.PoissonConfidence = setPoissonConfidence;
                            psrData.PoissonOutputPolygons = setPoissonOutputPolygons;
                            psrData.PoissonManifold = setPoissonManifold;
                            psrData.BPoissonDepth = BsetPoissonDepth;
                            psrData.BPoissonThreads = BsetNrThreads;
                            psrData.BPoissonMinDepth = BsetPoissonMinDepth;
                            psrData.BPoissonPointWeight = BsetPoissonPointWeight;
                            psrData.BPoissonScale = BsetPoissonScale;
                            psrData.BPoissonSolverDividede = BsetPoissonSolverDividede;
                            psrData.BPoissonIsoDivide = BsetPoissonIsoDivide;
                            psrData.BPoissonSamplesPerNode = BsetPoissonSamplesPerNode;
                            psrData.BPoissonDegree = BsetPoissonDegree;

                            psrData.BScaleNormals = setScaleNormals;

                            //psrData._neompData = NORMAL_ESTIMATION_OMP_DATA(); // empty data --> returns 0 for NrThreads
                            ImGui::EndTabItem();
                        }
                        if (ImGui::BeginTabItem("..."))
                        {
                            {
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildL", ImVec2(ImGui::GetContentRegionAvail().x * 0.5f, ImGui::GetContentRegionAvail().y), false, true);

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }
                            ImGui::SameLine();
                            {
                                ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                                ImGui::BeginChild("ChildR", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), false, true);

                                ImGui::EndChild();
                                ImGui::PopStyleVar();
                            }

                            ImGui::EndTabItem();
                        }
                    }
                    ImGui::EndTabBar();
                }

                ImGui::EndChild();
            }

            if (true)
            {
                /*std::strin items[] = { MyApp::FILEPATH_STORAGE::ultimate.c_str(),MyApp::FILEPATH_STORAGE::penultimate.c_str(),
                MyApp::FILEPATH_STORAGE::penpenultimate.c_str(), MyApp::FILEPATH_STORAGE::penpenpenultimate.c_str(), MyApp::FILEPATH_STORAGE::penpenpenpenultimate.c_str(), };
                */

                static std::string items[5] = { "","","","","" };
                static const char* itemsChar[5] = { items[0].c_str(), items[1].c_str(), items[2].c_str(), items[3].c_str(), items[4].c_str() };
                static int item_current = 0;
                MyApp::FILEPATH_STORAGE::ultimate = items[0]; // is this really needed?
                MyApp::FILEPATH_STORAGE::penultimate = items[1];
                MyApp::FILEPATH_STORAGE::penpenultimate = items[2];
                MyApp::FILEPATH_STORAGE::penpenpenultimate = items[3];
                MyApp::FILEPATH_STORAGE::penpenpenpenultimate = items[4];

                ImGui::BeginChild("button area pcl", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), true, ImGuiWindowFlags_AlwaysAutoResize);

                ImGui::BeginChild("buttons pcl", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y * 0.5f), false, ImGuiWindowFlags_AlwaysAutoResize);
                if (!(hasNrml || actvteClclt || (!normalArrayEmpty(items,5)))) {
                    ImGui::BeginDisabled();
                }
                if (ImGui::Button("Calculate")) {
                    ImGui::OpenPopup("Calculate - Confirmation");
                }
                if (!(hasNrml || actvteClclt || (!normalArrayEmpty(items, 5)))) {
                    ImGui::EndDisabled();
                }
                ImGui::SameLine();
                if (selected == 3 || selected == 2 || ((selected == 0) && (subselected == 1))) {
                    if (ImGui::Button("Normal ...")) {
                        ImGui::OpenPopup("Normal Estimation");
                    }
                }

                ImVec2 mnCntr = ImGui::GetMainViewport()->GetCenter();
                //ImVec2 popupSize = ImVec2(ImGui::GetWindowContentRegionMax().x, 10 * ImGui::GetWindowContentRegionMax().y);
                ImVec2 popupSize = ImVec2(600,200);
                ImVec2 popupSize2 = ImVec2(1200,400);
                
                ImGui::SetNextWindowSize(popupSize);
                ImGui::SetNextWindowPos(mnCntr, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
                if (ImGui::BeginPopupModal("Normal Estimation", NULL))
                {

                    ImGuiStyle& style = ImGui::GetStyle();
                    ImGui::PushStyleVar(style.FrameBorderSize, 1);
                    ImGuiWindowFlags window_flags = ImGuiWindowFlags_HorizontalScrollbar;
                    ImGui::BeginChild("NormalPopup", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y * 0.75f), true);
                    {
                        {
                            ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                            ImGui::BeginChild("ChildL", ImVec2(ImGui::GetContentRegionAvail().x * 0.5f, ImGui::GetContentRegionAvail().y), false, ImGuiWindowFlags_HorizontalScrollbar);
                            {
                                InputHelperInt(MyApp::NORMAL_ESTIMATION_OMP_DATA::BNrThreads, MyApp::NORMAL_ESTIMATION_OMP_DATA::NrThreads, "##NEOMPlsetNrThreads", "# threads");
                                ImGui::SameLine(); HelpMarker("Set the number of threads to use. Crashes most times when != 2. Choose wisely ...");
                            }
                            ImGui::Separator();
                            {
                                InputHelperFlt(MyApp::NORMAL_ESTIMATION_OMP_DATA::BViewPoint, MyApp::NORMAL_ESTIMATION_OMP_DATA::ViewPointX, "##NEOMPlsetViewPointX", "View point X");
                                ImGui::SameLine(); HelpMarker("Set the viewpoint.");
                            }
                            {
                                InputHelperFlt(MyApp::NORMAL_ESTIMATION_OMP_DATA::BViewPoint, MyApp::NORMAL_ESTIMATION_OMP_DATA::ViewPointY, "##NEOMPlsetViewPointY", "View point Y");
                                ImGui::SameLine(); HelpMarker("Set the viewpoint.");
                            }
                            {
                                InputHelperFlt(MyApp::NORMAL_ESTIMATION_OMP_DATA::BViewPoint, MyApp::NORMAL_ESTIMATION_OMP_DATA::ViewPointZ, "##NEOMPlsetViewPointZ", "View point Z");
                                ImGui::SameLine(); HelpMarker("Set the viewpoint.");
                            }
                            ImGui::Separator();
                            {
                                InputHelperDbl(MyApp::NORMAL_ESTIMATION_OMP_DATA::BRadiusSearch, MyApp::NORMAL_ESTIMATION_OMP_DATA::RadiusSearch, "##NEOMPlsetRadiusSearch", "Radius search");
                                ImGui::SameLine(); HelpMarker("Set the sphere radius that is to be used for determining the nearest neighbors used for the feature estimation.");
                            }
                            {
                                InputHelperInt(MyApp::NORMAL_ESTIMATION_OMP_DATA::BKSearch, MyApp::NORMAL_ESTIMATION_OMP_DATA::KSearch, "##NEOMPlsetKSearch", "K search");
                                ImGui::SameLine(); HelpMarker("Set the number of k nearest neighbors to use for the feature estimation.");
                            }

                            ImGui::EndChild();
                            ImGui::PopStyleVar();
                        }
                        ImGui::SameLine();
                        {
                            ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
                            ImGui::BeginChild("ChildR", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), false, ImGuiWindowFlags_HorizontalScrollbar);

                            ImGui::Checkbox("Sensor origin as view point", &MyApp::NORMAL_ESTIMATION_OMP_DATA::BUseSensorOriginAsViewPoint);
                            ImGui::SameLine(); HelpMarker("Sets whether the sensor origin or a user given viewpoint should be used.");

                            ImGui::EndChild();
                            ImGui::PopStyleVar();
                        }
                    }
                    ImGui::EndChild();
                    ImGui::PopStyleVar();

                    ImGui::BeginChild("NormalPopup area Ok Cancel", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), true);
                    if (ImGui::Button("OK", ImVec2(ImGui::GetContentRegionAvail().x * 0.5f, ImGui::GetContentRegionAvail().y))) {
                        ImGui::OpenPopup("Normal Estimation - File Setup");
                    }
                    ImGui::SetItemDefaultFocus();
                    ImGui::SameLine();
                    if (ImGui::Button("Cancel", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y))) {
                        //hasNrml = false;
                        ImGui::CloseCurrentPopup();
                    }

                    /*if (setScaleNormals) {
                        ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "Normals Scaled with curvature!");
                    }*/

                    bool unused_open = true;
                    std::string cldFlNmNrm = "";
                    ImGui::SetNextWindowSize(popupSize);
                    ImGui::SetNextWindowPos(mnCntr, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
                    if (ImGui::BeginPopupModal("Normal Estimation - File Setup", &unused_open))
                    {
                        ImGui::BeginChild("NormalEstimationPopup", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y * 0.75f), true);

                        static bool BuseStdNm = false;
                        static std::string cldNrmlFlNm = inputCldFle;
                        if (BuseStdNm)
                            ImGui::BeginDisabled();
                        ImGui::InputText("Please choose a filename", &cldNrmlFlNm);
                        if (BuseStdNm)
                            ImGui::EndDisabled();
                        ImGui::Separator();
                        ImGui::Spacing();
                        ImGui::Checkbox("use default filename", &BuseStdNm);
                        ImGui::SameLine();
                        ImGui::SameLine(); HelpMarker("Use default filename (i.e. name of PCD file).");
                        ImGui::EndChild();
                        ImGui::BeginChild("NormalEstimationPopup area Ok Cancel", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), true);
                        if (ImGui::Button("Ok", ImVec2(ImGui::GetContentRegionAvail().x * 0.5f, ImGui::GetContentRegionAvail().y))) {
                            if (selected == 3 || selected == 2 || ((selected == 0) && (subselected == 1)))
                            {
                                if (BuseStdNm) {
                                    cldNrmlFlNm = inputCldFle;
                                }
                                if (true) {
                                    if (PCLNormalEstimationOMP((cldNrmlFlNm), outputCldPth, (inputCldPth + "\\" + inputCldFle + "." + inputCldFleDType)))
                                    {
                                        log.AddLog("[%05d] %s Normal Estimation stored at:\n\n%s\n\n", ImGui::GetFrameCount(), categories[0],
                                            (outputCldPth + "\\" + cldNrmlFlNm + "_NE.pcd").c_str());
                                        // update array of NE
                                        if (BuseStdNm)
                                            updateFilepathStorage(items, inputCldFle);
                                        else
                                            updateFilepathStorage(items, (cldNrmlFlNm));
                                        /// normals can be loaded from file therefore at this moment the variable to look for the normal
                                        /// is set to the ouput path of the normal estimation but can be overwritten anywhere
                                        normalCldFle = cldNrmlFlNm;
                                        normalCldPth = outputCldPth;
                                        normalCldFleDType = inputCldFleDType;
                                    }
                                    else {
                                        log.AddLog("[%05d] %s Normal Estimation impossible!\n", ImGui::GetFrameCount(), categories[2]);
                                    }
                                    hasNrml = true; // enable Calculate button
                                }
                                else {
                                    log.AddLog("[%05d] %s Input cloud string too short!\n", ImGui::GetFrameCount(), categories[2]);
                                }
                            }
                            ImGui::ClosePopupToLevel(0, true); // comment
                        }
                        ImGui::SameLine();
                        if (ImGui::Button("Cancel", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y))) {
                            ImGui::ClosePopupToLevel(0, true);
                        }
                        ImGui::EndChild();
                        ImGui::EndPopup();
                    }
                    ImGui::EndChild();
                    ImGui::EndPopup();
                }

                ImGui::SetNextWindowSize(popupSize);
                ImGui::SetNextWindowPos(mnCntr, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
                if (ImGui::BeginPopupModal("Calculate - Confirmation", NULL))
                {
                    ImGui::BeginChild("CalculatePopup", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y * 0.75f), true);
                    ImGui::Text("The computation time can last up to several minutes. Continue?");
                    if (setScaleNormals) {
                        ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "\n\nNormals Scaled with curvature!");
                    }
                    ImGui::EndChild();

                    ImGui::BeginChild("CalculatePopup Area Yes No", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), true);
                    bool unused_open = true;
                    static bool BdelNEOMPFl = false;
                    static bool BuseStdNm = false;
                    static std::string cldResFlNm = inputCldFle;
                    ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(2 / 7.0f, 0.6f, 0.6f));
                    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(2 / 7.0f, 0.7f, 0.7f));
                    ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(2 / 7.0f, 0.8f, 0.8f));
                    if (ImGui::Button("Yes", ImVec2(ImGui::GetContentRegionAvail().x * 0.5f, ImGui::GetContentRegionAvail().y))) {
                        ImGui::OpenPopup("Calculate - File Setup");
                    }
                    ImGui::PopStyleColor(3);

                    ImGui::SetNextWindowSize(popupSize);
                    ImGui::SetNextWindowPos(mnCntr, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
                    if (ImGui::BeginPopupModal("Calculate - File Setup", &unused_open))
                    {
                        if (selected == 2 || selected == 3 || ((selected == 0) && (subselected == 1))) {
                            ImGui::BeginChild("CalculatePopup File Setup", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y * 0.75f), true);
                            if (BuseStdNm)
                                ImGui::BeginDisabled();
                            ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.8f);
                            ImGui::InputText("filename", &cldResFlNm);
                            if (BuseStdNm)
                                ImGui::EndDisabled();
                            ImGui::Separator();

                            static bool BuseCombo = true;
                            static bool BuseBrowser = false;
                            ImGui::Checkbox("##use combo for normals", &BuseCombo);
                            ImGui::SameLine();
                            if (!BuseCombo) {
                                ImGui::BeginDisabled();
                            }
                            ImGui::Combo("NE file", &item_current, itemsChar, IM_ARRAYSIZE(itemsChar));
                            ImGui::SameLine();
                            ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
                            HelpMarker("Choose from the last 5 normal estimations.");
                            if (!BuseCombo) {
                                ImGui::EndDisabled();
                            }
                            ImGui::Checkbox("##use file browser for normals", &BuseBrowser);
                            ImGui::SameLine();
                            if (!BuseBrowser) {
                                ImGui::BeginDisabled();
                            }    
                            if (ImGui::Button("Browse NE file")) {
                                ImGui::OpenPopup("Calculate - Normal file");
                            }
                            if (!BuseBrowser) {
                                ImGui::EndDisabled();
                            }

                            /*                            float availReg = ImGui::GetContentRegionAvail().x * 0.8f;
                            static bool BuseCombo = true;
                            static bool BuseBrowser = false;

                            ////const char* items[] = { "AAAA", "BBBB", "CCCC", "DDDD", "EEEE", "FFFF", "GGGG", "HHHH", "IIII", "JJJJ", "KKKK", "LLLLLLL", "MMMM", "OOOOOOO" };
                            //static int item_current_idx = 0; // Here we store our selection data as an index.
                            //const char* combo_preview_value = itemsChar[item_current_idx];  // Pass in the preview value visible before opening the combo (it could be anything)
                            //ImGui::SetNextItemWidth(availReg / 3);
                            //if (ImGui::BeginCombo("NE file", combo_preview_value))
                            //{
                            //    for (int n = 0; n < IM_ARRAYSIZE(itemsChar); n++)
                            //    {
                            //        const bool is_selected = (item_current_idx == n);
                            //        if (ImGui::Selectable(itemsChar[n], is_selected)) {
                            //            item_current_idx = n;
                            //            BuseCombo = true;
                            //        }

                            //        // Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
                            //        if (is_selected)
                            //            ImGui::SetItemDefaultFocus();
                            //    }
                            //    ImGui::EndCombo();
                            //}
                            //ImGui::SameLine();
                            //HelpMarker("Choose from the last 5 normal estimations.");

                            //static ImGuiComboFlags flags = 0;
                            //const char* items2[] = { "AAAA", "BBBB", "CCCC", "DDDD", "EEEE", "FFFF", "GGGG", "HHHH", "IIII", "JJJJ", "KKKK", "LLLLLLL", "MMMM" };
                            //static int item_current_idx = 0; // Here we store our selection data as an index.
                            //const char* combo_preview_value = items2[item_current_idx];  // Pass in the preview value visible before opening the combo (it could be anything)
                            //if (ImGui::BeginCombo("combo 1", combo_preview_value, flags))
                            //{
                            //    for (int n = 0; n < IM_ARRAYSIZE(items2); n++)
                            //    {
                            //        const bool is_selected = (item_current_idx == n);
                            //        if (ImGui::Selectable(items2[n], is_selected))
                            //            item_current_idx = n;

                            //        // Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
                            //        if (is_selected)
                            //            ImGui::SetItemDefaultFocus();
                            //    }
                            //    ImGui::EndCombo();
                            //}

                            ImGui::Checkbox("##use combo for normals", &BuseCombo);
                            ImGui::SameLine();
                            if (!BuseCombo) {
                                ImGui::BeginDisabled();
                            }
                            ImGui::SetNextItemWidth(availReg/3);
                            ImGui::Combo("NE file", &item_current, itemsChar, IM_ARRAYSIZE(itemsChar));
                            ImGui::SameLine();
                            ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
                            HelpMarker("Choose from the last 5 normal estimations.");
                            if (!BuseCombo) {
                                ImGui::EndDisabled();
                            }

                            //ImGui::SameLine();
                            ////ImGui::SetNextItemWidth(availReg / 3);
                            //ImGui::SetNextItemWidth(200);
                            //ImGui::Text("or");

                            //ImDrawList* draw_list = ImGui::GetWindowDrawList();
                            //if (!inputCldFle.empty())
                            //{
                            //    ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.8f);
                            //    ImGui::TextWrapped((inputCldFle).c_str());
                            //    draw_list->AddRect(ImVec2(ImGui::GetItemRectMin().x - 3, ImGui::GetItemRectMin().y - 3),
                            //        ImVec2(ImGui::GetItemRectMax().x + 3, ImGui::GetItemRectMax().y + 3), IM_COL32(200, 200, 200, 255));
                            //}
                            //
                            //    

                            ImGui::Checkbox("##use file browser for normals", &BuseBrowser);
                            ImGui::SameLine();
                            if (!BuseBrowser) {
                                ImGui::BeginDisabled();
                            }
                                
                            ImGui::SetNextItemWidth(availReg/3);
                            //if (!BuseCombo) {
                            //    ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(2 / 7.0f, 0.8f, 0.8f));
                            //}
                            if (ImGui::Button("Browse NE file")) {
                                ImGui::OpenPopup("Calculate - Normal file");
                            }
                            //if (!BuseCombo) {
                            //    ImGui::PopStyleColor(1);
                            //}
                            if (!BuseBrowser) {
                                ImGui::EndDisabled();
                            }*/

                            ImGui::SetNextWindowSize(popupSize);
                            ImGui::SetNextWindowPos(mnCntr, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
                            if (ImGui::BeginPopupModal("Calculate - Normal file", NULL))
                            {
                                ImGui::BeginChild("input normal file", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y * 0.5f), false, ImGuiWindowFlags_AlwaysAutoResize);
                                (normalCldPth == "") ? initPath = "." : initPath = normalCldPth + "\\"; // overwrite lookup path for dear im gui file dialog
                                ImGui::SetNextWindowSize(popupSize2);
                                ImGui::SetNextWindowPos(mnCntr, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
                                ImGuiFileDialog::Instance()->OpenDialog("ChooseFileDlgKey2", "Choose Normal file", ".pcd", initPath);
                                if (ImGuiFileDialog::Instance()->Display("ChooseFileDlgKey2", ImGuiWindowFlags_NoMove))
                                {
                                    if (ImGuiFileDialog::Instance()->IsOk()) {
                                        // filepath
                                        inputCldPth = ImGuiFileDialog::Instance()->GetCurrentPath();
                                        // filename
                                        std::string inputCldFlPth = ImGuiFileDialog::Instance()->GetFilePathName();
                                        int lenFull = inputCldFlPth.length();
                                        int lenFullNoDataType = lenFull - 5; // .pcd\0
                                        int lenFullWDataType = lenFull - 1; // .pcd\0
                                        int lenDir = inputCldPth.length();
                                        normalCldPth = inputCldPth;
                                        normalCldFle = inputCldFlPth.substr(lenDir + 1, (lenFullNoDataType - lenDir));
                                        normalCldFleDType = inputCldFlPth.substr(lenFullNoDataType + 2, 3);
                                        log.AddLog("[%05d] %s Browse File from %s\\%s.%s\n\n", ImGui::GetFrameCount(), categories[0], normalCldPth.c_str(), normalCldFle.c_str(), normalCldFleDType.c_str());
                                        ImGui::CloseCurrentPopup();
                                    }
                                    ImGui::CloseCurrentPopup();
                                    ImGuiFileDialog::Instance()->Close();
                                    //BuseCombo = false;
                                }

                                ImGui::EndChild();
                                ImGui::EndPopup();
                            }

                            

                            ImGui::Separator();
                            ImGui::Spacing();
                            ImGui::Checkbox("delete normal file afterwards", &BdelNEOMPFl);
                            ImGui::SameLine();
                            ImGui::SameLine(); HelpMarker("Delete '_NE' file used for calculation. (only when success)");
                            ImGui::Spacing();
                            ImGui::Checkbox("use default filename", &BuseStdNm);
                            ImGui::SameLine();
                            ImGui::SameLine(); HelpMarker("Use default filename (i.e. name of PC or NE file).");
                            ImGui::EndChild();
                            ImGui::BeginChild("CalculatePopup File Setup Ok Cancel", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), true);
                            if (ImGui::Button("Ok", ImVec2(ImGui::GetContentRegionAvail().x * 0.5f, ImGui::GetContentRegionAvail().y))) {
                                

                                if (!(BuseCombo != BuseBrowser)) {
                                    ImGui::OpenPopup("Normal Estimation - Info");
                                }
                                else
                                {
                                    static std::string normalCldPthFull;
                                    (BuseCombo) ? normalCldPthFull = (normalCldPth + "\\" + items[item_current] + "_NE." + normalCldFleDType) : normalCldPthFull = (normalCldPth + "\\" + normalCldFle + "." + normalCldFleDType);
                                    // Region Growing
                                    if (((selected == 0) && (subselected == 1))) {
                                        if (PCLRGSegmentation(rgData, cldResFlNm, outputCldPth, (inputCldPth + "\\" + inputCldFle + "." + inputCldFleDType), normalCldPthFull)) {
                                            log.AddLog("[%05d] %s Region Growing Segmentation done!\n", ImGui::GetFrameCount(), categories[0]);
                                            log.AddLog("[%05d] %s PCD file stored at:\n\n%s\n\n", ImGui::GetFrameCount(), categories[0],
                                                (outputCldPth + "\\" + cldResFlNm + "_RG_RGB.pcd").c_str());
                                        }               
                                        else {
                                            log.AddLog("[%05d] %s RG Segmentation impossible!\n\n", ImGui::GetFrameCount(), categories[2]);
                                        }
                                    }
                                    // Boundary Estimation
                                    if (selected == 2)
                                    {
                                        if (BuseStdNm) {
                                            cldResFlNm = items[item_current];
                                        }
                                        //if (PCLPoissonSurfaceReconstruction(psrData, cldMshFlNm, outputCldPth, (inputCldPth + "\\" + items[item_current] + "." + inputCldFleDType), (outputCldPth + "\\" + items[item_current] + "_NE." + inputCldFleDType))) {
                                        if (PCLBoundaryEstimation(beData, cldResFlNm, outputCldPth, (inputCldPth + "\\" + inputCldFle + "." + inputCldFleDType), normalCldPthFull)) {
                                            log.AddLog("[%05d] %s Boundary Estimation done!\n", ImGui::GetFrameCount(), categories[0]);
                                            log.AddLog("[%05d] %s PCD file stored at:\n\n%s\n\n", ImGui::GetFrameCount(), categories[0],
                                                (outputCldPth + "\\" + cldResFlNm + "_BE.pcd").c_str());
                                            if (BuseCombo)
                                            {
                                                log.AddLog("[%05d] %s NE file used from combo: %s\n\n", ImGui::GetFrameCount(), categories[0], itemsChar[item_current]);
                                            }
                                            else if (BuseBrowser) {
                                                log.AddLog("[%05d] %s NE file used from file: %s\n\n", ImGui::GetFrameCount(), categories[0], normalCldFle);
                                            }
                                            if (BdelNEOMPFl && BuseCombo) {
                                                log.AddLog("[%05d] %s NE file removed: %s\n\n", ImGui::GetFrameCount(), categories[0], itemsChar[item_current]);
                                                std::remove((outputCldPth + "\\" + items[item_current] + "_NE." + inputCldFleDType).c_str());
                                                updateFilepathStorageRmUltmt(items);
                                            }
                                        }
                                        else {
                                            log.AddLog("[%05d] %s Boundary Estimation impossible!\n\n", ImGui::GetFrameCount(), categories[2]);
                                        }
                                    }
                                    // Poisson Surface Reconstruction
                                    if (selected == 3)
                                    {
                                        if (BuseStdNm) {
                                            cldResFlNm = items[item_current];
                                        }
                                        try {
                                            if (PCLPoissonSurfaceReconstruction(psrData, cldResFlNm, outputCldPth, (inputCldPth + "\\" + inputCldFle + "." + inputCldFleDType), normalCldPthFull)) {
                                                log.AddLog("[%05d] %s Poisson Surface Reconstruction done!\n", ImGui::GetFrameCount(), categories[0]);
                                                log.AddLog("[%05d] %s Mesh file stored at:\n\n%s\n\n", ImGui::GetFrameCount(), categories[0],
                                                    (outputCldPth + "\\" + cldResFlNm + "_PSR.ply").c_str());

                                                if (BuseCombo)
                                                {
                                                    log.AddLog("[%05d] %s NE file used from combo: %s\n\n", ImGui::GetFrameCount(), categories[0], itemsChar[item_current]);
                                                }
                                                else if (BuseBrowser) {
                                                    log.AddLog("[%05d] %s NE file used from file: %s\n\n", ImGui::GetFrameCount(), categories[0], normalCldFle);
                                                }
                                                if (BdelNEOMPFl && BuseCombo) {
                                                    log.AddLog("[%05d] %s NE file removed: %s\n\n", ImGui::GetFrameCount(), categories[0], itemsChar[item_current]);
                                                    std::remove((outputCldPth + "\\" + items[item_current] + "_NE." + inputCldFleDType).c_str());
                                                    updateFilepathStorageRmUltmt(items);
                                                }
                                            }
                                            else {
                                                log.AddLog("[%05d] %s Poisson Surface Reconstruction impossible!\n\n", ImGui::GetFrameCount(), categories[2]);
                                            }
                                        }
                                        catch (const std::bad_array_new_length& bae) {
                                            log.AddLog("[%05d] %s Poisson Surface Reconstruction impossible!\n\n%s\n\n", ImGui::GetFrameCount(), categories[2], bae.what());
                                        }
                                    }
                                    ImGui::ClosePopupToLevel(0, true);
                                }

                                
                            }
                            ImGui::SameLine();
                            if (ImGui::Button("Cancel", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y))) {
                                ImGui::ClosePopupToLevel(0, true);
                            }
                            if (setScaleNormals) {
                                ImGui::SameLine();
                                ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "Normals Scaled with curvature!");
                            }


                            ImGui::SetNextWindowSize(popupSize);
                            ImGui::SetNextWindowPos(mnCntr, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
                            if (ImGui::BeginPopupModal("Normal Estimation - Info", &unused_open))
                            {
                                ImGui::BeginChild("NE warning combo and browser", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y * 0.75f), true);
                                ImGui::Text("You can't have both - NE from combo box and file!\n\n");
                                ImGui::EndChild();
                                ImGui::BeginChild("NE warning combo and browser area Ok Cancel", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), true);
                                if (ImGui::Button("Ok", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y))) {
                                    BuseCombo = true;
                                    BuseBrowser = false;
                                    ImGui::CloseCurrentPopup();
                                }
                                ImGui::EndChild();
                                ImGui::EndPopup();
                            }

                            ImGui::EndChild();
                        }
                        else if (((selected == 0) && (subselected == 0)) || selected == 1) {
                            ImGui::BeginChild("CalculatePopup File Setup 1", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y * 0.75f), true);
                            if (BuseStdNm)
                                ImGui::BeginDisabled();
                            ImGui::InputText("filename", &cldResFlNm);
                            if (BuseStdNm)
                                ImGui::EndDisabled();
                            ImGui::Separator();
                            ImGui::Spacing();
                            ImGui::Checkbox("use default filename", &BuseStdNm);
                            ImGui::SameLine();
                            ImGui::SameLine(); HelpMarker("Use default filename (i.e. name of PCD or NE file).");
                            ImGui::EndChild();
                            ImGui::BeginChild("CalculatePopup File Setup Ok Cancel 1", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y), true);
                            if (ImGui::Button("Ok", ImVec2(ImGui::GetContentRegionAvail().x * 0.5f, ImGui::GetContentRegionAvail().y))) {
                                if (BuseStdNm) {
                                    cldResFlNm = items[item_current];
                                }
                                if (selected == 0) {
                                    if (PCLSACSegmentation(sacData, cldResFlNm, outputCldPth, (inputCldPth + "\\" + inputCldFle + "." + inputCldFleDType))) {
                                        log.AddLog("[%05d] %s SAC Segmentation done!\n", ImGui::GetFrameCount(), categories[0]);
                                        log.AddLog("[%05d] %s PCD file stored at:\n\n%s\n\n", ImGui::GetFrameCount(), categories[0],
                                            (outputCldPth + "\\" + cldResFlNm + "_SAC.pcd").c_str());
                                    }
                                    else {
                                        log.AddLog("[%05d] %s SAC Segmentation impossible!\n\n", ImGui::GetFrameCount(), categories[2]);
                                    }
                                }
                                if (selected == 1) {
                                    if (PCLStatisticalOutlierRemoval(sorData, cldResFlNm, outputCldPth, (inputCldPth + "\\" + inputCldFle + "." + inputCldFleDType))) {
                                        log.AddLog("[%05d] %s SAC Segmentation done!\n", ImGui::GetFrameCount(), categories[0]);
                                        log.AddLog("[%05d] %s PCD file stored at:\n\n%s\n\n", ImGui::GetFrameCount(), categories[0],
                                            (outputCldPth + "\\" + cldResFlNm + "_SOR.pcd").c_str());
                                    }
                                    else {
                                        log.AddLog("[%05d] %s SAC Segmentation impossible!\n\n", ImGui::GetFrameCount(), categories[2]);
                                    }
                                }
                                ImGui::ClosePopupToLevel(0, true);
                            }
                            ImGui::SameLine();
                            if (ImGui::Button("Cancel", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y))) {
                                ImGui::ClosePopupToLevel(0, true);
                            }
                            ImGui::EndChild();
                        }

                        ImGui::EndPopup();
                    }

                    ImGui::SetItemDefaultFocus();
                    ImGui::SameLine();
                    ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0 / 7.0f, 0.6f, 0.6f));
                    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0 / 7.0f, 0.7f, 0.7f));
                    ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(0 / 7.0f, 0.8f, 0.8f));
                    if (ImGui::Button("No", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y))) {
                        ImGui::CloseCurrentPopup();
                    }
                    ImGui::PopStyleColor(3);
                    ImGui::EndChild();
                    ImGui::EndPopup();
                }

                ImGui::EndChild();
                ImGui::EndChild();
            }

            ImGui::EndGroup();

        }

        ImGui::End();
    }
}
