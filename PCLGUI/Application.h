#pragma once
#include <iostream>
#include <math.h>

#define _USE_MATH_DEFINES

namespace MyApp
{
    // Dieses struct dient dazu, alle Informationen zu sammeln, die durch den Aufruf eines
    // Popup-Fensters zur Eingabe von Parametern f�r die Berechnung von Normalen entstehen.
    // Beispiel ist PoissonSurfaceReconstruction.
    // Das struct speichert die Werte bis zum Ende des Programms und sollte daher nur an
    // gew�nschter Stelle bearbeitet werden k�nnen.
    struct NORMAL_ESTIMATION_OMP_DATA {
        static std::string cldNrmlFlPth;
        static int NrThreads;

        static float ViewPointX;
        static float ViewPointY;
        static float ViewPointZ;

        static double RadiusSearch;
        static int KSearch;
        static bool BNrThreads;

        static bool BViewPoint;
        static bool BUseSensorOriginAsViewPoint;

        static bool BRadiusSearch;
        static bool BKSearch;
    };

    struct FILEPATH_STORAGE {
        static std::string ultimate;
        static std::string penultimate;
        static std::string penpenultimate;
        static std::string penpenpenultimate;
        static std::string penpenpenpenultimate;
    };

    void RenderUI();
}
